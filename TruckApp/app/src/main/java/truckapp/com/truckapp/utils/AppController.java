package truckapp.com.truckapp.utils;


import android.app.Application;


public class AppController extends Application {

    public static final String TAG = "AOTAG";

    public static AppController appController;

    @Override
    public void onCreate() {
        super.onCreate();
        appController = this;
        // UNIVERSAL IMAGE LOADER SETUP

//        try {
//            StrictModeWrapper.init(this);
//        }
//        catch(Throwable throwable) {
//            Log.v("StrictMode", "... cc. Punting...");
//        }

    }

    public static synchronized AppController getInstance() {
        return appController;
    }

    public String truckId = "";


}
