package truckapp.com.truckapp.model.responseobjects.login;

/**
 * Created by ramya on 12/29/2015.
 */
public class ApplicationUserModel {

   String UserId;
    String UserName;
    String Password;
    String EmployeeId;
    String Active;
    String EmployeeName;
    String RoleName;
    String Branch;
    String LOBName;
    String BAPC_Code;
    String ATPC_Code;
    String Sales_Scoring;
    String Omnisys_Code;






    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(String employeeId) {
        EmployeeId = employeeId;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getLOBName() {
        return LOBName;
    }

    public void setLOBName(String LOBName) {
        this.LOBName = LOBName;
    }

    public String getBAPC_Code() {
        return BAPC_Code;
    }

    public void setBAPC_Code(String BAPC_Code) {
        this.BAPC_Code = BAPC_Code;
    }

    public String getATPC_Code() {
        return ATPC_Code;
    }

    public void setATPC_Code(String ATPC_Code) {
        this.ATPC_Code = ATPC_Code;
    }

    public String getSales_Scoring() {
        return Sales_Scoring;
    }

    public void setSales_Scoring(String sales_Scoring) {
        Sales_Scoring = sales_Scoring;
    }

    public String getOmnisys_Code() {
        return Omnisys_Code;
    }

    public void setOmnisys_Code(String omnisys_Code) {
        Omnisys_Code = omnisys_Code;
    }
}
