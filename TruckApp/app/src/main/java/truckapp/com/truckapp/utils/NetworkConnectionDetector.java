package truckapp.com.truckapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Nishanth on 15-07-2015.
 */
public class NetworkConnectionDetector extends BroadcastReceiver {

    private Context context;

    // Constructor
    public NetworkConnectionDetector(Context context) {
        this.context = context;
    }

    public boolean connectionStatus() {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.v("Network State :- ", info[i].getTypeName());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

    }
}
