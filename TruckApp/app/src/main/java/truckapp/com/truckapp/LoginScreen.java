package truckapp.com.truckapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import truckapp.com.truckapp.base.BaseActivity;
import truckapp.com.truckapp.model.requestobjects.LoginRequest;
import truckapp.com.truckapp.model.responseobjects.LoginModel;
import truckapp.com.truckapp.model.responseobjects.TabBarStatusModel;
import truckapp.com.truckapp.service.servicefactory.ServiceFactory;
import truckapp.com.truckapp.utils.AppController;
import truckapp.com.truckapp.utils.CommonUtils;
import truckapp.com.truckapp.utils.Constant;
import truckapp.com.truckapp.utils.ErrorMessage;
import truckapp.com.truckapp.utils.NetworkConnectionDetector;

public class LoginScreen extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.dont_have_account_txt)
    TextView dont_have_account_txt;
    @Bind(R.id.login_username_txt)
    TextView username_txt;
    @Bind(R.id.login_password_txt)
    TextView password_txt;
    @Bind(R.id.login_img)
    ImageView login_btn;

    NetworkConnectionDetector networkConnectionDetector;

    LoginRequest loginRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        ButterKnife.bind(this);

        networkConnectionDetector = new NetworkConnectionDetector(this);
        dont_have_account_txt.setText(Html.fromHtml("<p>DON'T HAVE AN ACCOUNT?<font size='7'color='#883e33'><bold>  SIGN UP</bold></font></p>"));

        login_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_img:
                username_txt.setText("790");
                password_txt.setText("password");
                if (username_txt.getText().toString().equals("")) {
                    CommonUtils.ShowToast(this, "Please enter username and try again");
                } else if (password_txt.getText().toString().equals("")) {
                    CommonUtils.ShowToast(this, "Please enter password and try again");
                } else {
                    AppController.getInstance().truckId = username_txt.getText().toString();
                    if (networkConnectionDetector.connectionStatus()) {
                        LoginAction();
                    } else {
                        CommonUtils.ShowToast(this, "Network Connection is not response properly. Please check your connection.");
                    }
                    //startActivity(new Intent(this, HomeScreen.class));
                }
                break;
        }
    }

    public void LoginAction() {


        // showProgress(true);
        showProgress("Loading");


        ServiceFactory.get().Login(username_txt.getText().toString(), password_txt.getText().toString(), new Callback<LoginModel>() {
            @Override
            public void success(LoginModel loginModel, Response response) {
                // success!

                Log.v(Constant.TAG, "--->" + loginModel.getTrucks().getId());
                //startActivity(new Intent(LoginScreen.this, HomeScreen.class));
                //
                getWorkFlow();
                //   mLogin=new MLogin(BLogin.this,loginModel);

//                if (mProgressDialog.isShowing()) {
//
//                    mProgressDialog.dismiss();
//
//                }


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                showAlert(ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });

    }

    private void getWorkFlow() {

        showProgress("Loading");

        ServiceFactory.get().OrderWorkFlow("DRIVER", new Callback<List<TabBarStatusModel>>() {
            @Override
            public void success(List<TabBarStatusModel> workFlowModelReponse, Response response) {
                // success!

                Log.v(Constant.TAG, "workFlowModelResponse --->" + workFlowModelReponse.size());

                for (int i = 0; i < workFlowModelReponse.size(); i++) {
                    Log.v(Constant.TAG, i + " Status Dec :- " + workFlowModelReponse.get(i).getStatusDesc());
                    Log.v(Constant.TAG, i + " Status Id :- " + workFlowModelReponse.get(i).getStatusId());
                    Log.v(Constant.TAG, i + " To Status Dec :- " + workFlowModelReponse.get(i).getToStatusId());
                    Log.v(Constant.TAG, i + " Id :- " + workFlowModelReponse.get(i).getId());
                }
                openHomeScreenAction(workFlowModelReponse);
                // startActivity(new Intent(LoginScreen.this, HomeScreen.class));
                //
                //  getWorkFlow();
                //   mLogin=new MLogin(BLogin.this,loginModel);

                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                    Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                    Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                }

                showAlert("Error :- " + ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });
    }

    private void openHomeScreenAction(List<TabBarStatusModel> workFlowModelReponse) {
        finish();
        Intent openHomeScreen = new Intent(this, HomeScreen.class);
        openHomeScreen.putExtra("TabList", (Serializable) workFlowModelReponse);
        startActivity(openHomeScreen);
    }

}
