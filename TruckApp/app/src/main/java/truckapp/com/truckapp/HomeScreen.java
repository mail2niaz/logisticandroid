package truckapp.com.truckapp;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dmax.dialog.SpotsDialog;
import truckapp.com.truckapp.fragment.DeliveredFragment;
import truckapp.com.truckapp.fragment.OrderFragment;
import truckapp.com.truckapp.fragment.ReadyShippingFragment;
import truckapp.com.truckapp.fragment.ShippedFragment;
import truckapp.com.truckapp.model.StaticOrderModel;
import truckapp.com.truckapp.model.responseobjects.TabBarStatusModel;
import truckapp.com.truckapp.utils.Constant;


public class HomeScreen extends ActionBarActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;
    public static boolean readyShippingFlag = false;
    SmartTabLayout viewPagerTab;

    public AlertDialog sportDialog;

    public static List<TabBarStatusModel> myList;
    public static int currentSelectedTab = 0;

    public static final ArrayList<StaticOrderModel> OrderWholeArrayList = new ArrayList<StaticOrderModel>() {
    };

    private String TAG = "Truck";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        sportDialog = new SpotsDialog(this);
        Log.v(Constant.TAG, "Entered Home Screen:- ");


        myList = (List<TabBarStatusModel>) getIntent().getSerializableExtra("TabList");

        for (int i = 0; i < myList.size(); i++) {
            Log.v(Constant.TAG, "Before myList :- " + myList.get(i).getStatusDesc());
            Log.v(Constant.TAG, "Before myList Id :- " + myList.get(i).getId());
        }

        // ----------------- Start Sort the array -----------------

        Collections.sort(myList, new Comparator<TabBarStatusModel>() {
            @Override
            public int compare(TabBarStatusModel lhs, TabBarStatusModel rhs) {

                return Integer.valueOf(lhs.getStatusSeq()).compareTo(rhs.getStatusSeq());
            }
        });

        // ----------------- End Sort the array -----------------

        Log.v(Constant.TAG, "TabList :- " + myList.size());

        for (int i = 0; i < myList.size(); i++) {
            Log.v(Constant.TAG, "myList :- " + myList.get(i).getStatusDesc());
            Log.v(Constant.TAG, "myList Id :- " + myList.get(i).getId());
        }


        insertAdapter();

        FragmentPagerItems pages = new FragmentPagerItems(this);
        for (int i = 0; i < myList.size(); i++) {
            pages.add(FragmentPagerItem.of(myList.get(i).getStatusDesc(), OrderFragment.class));
        }

        FragmentPagerItemAdapter adapter = new FragmentPagerItemExtendsAdapter(
                getSupportFragmentManager(), pages);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        //   viewPager.setOffscreenPageLimit(0);

        viewPager.setAdapter(adapter);


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.v(TAG, "Position :- " + position);
                currentSelectedTab = position;
                // CommonUtils.ShowToast(getApplicationContext(), "onPageSelected position - " + position);
                viewPager.getAdapter().notifyDataSetChanged();
                //  viewPager.getAdapter().getItemPosition(position).refresh();
                // resetTabBarIcon(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //CommonUtils.ShowToast(getApplicationContext(), "state - " + state);
            }

        });

        viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        setSupportActionBar(toolbar);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

// Old Tab Bar Code
/*
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        setSupportActionBar(toolbar);
        //  drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();*/


        //getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getSupportActionBar().setTitle("Social");
       /* viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.v(TAG, "Position :- " + position);
                viewPager.getAdapter().notifyDataSetChanged();
                // resetTabBarIcon(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(viewPager);*/
        //setupTabIcons();
        //resetTabBarIcon(0);

    }

    private void insertAdapter() {
        OrderWholeArrayList.clear();
        StaticOrderModel firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("13, Jan");
        firstorderModel.setDeliveryAddress("6545 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("04:30");
        firstorderModel.setOrderId("7856324");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setStatus("r");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(0, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("14, Jan");
        firstorderModel.setDeliveryAddress("1234 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("05:30");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setOrderId("98756");
        firstorderModel.setStatus("s");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(1, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("15, Jan");
        firstorderModel.setDeliveryAddress("123456 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("06:30");
        firstorderModel.setOrderId("5678");
        firstorderModel.setStatus("o");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(2, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("16, Jan");
        firstorderModel.setOrderId("1234");
        firstorderModel.setDeliveryAddress("6545 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("07:30");
        firstorderModel.setStatus("o");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(3, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("18, Jan");
        firstorderModel.setDeliveryAddress("7894 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("08:30");
        firstorderModel.setStatus("o");
        firstorderModel.setOrderId("12541");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(4, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("18, Jan");
        firstorderModel.setDeliveryAddress("7894 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("08:30");
        firstorderModel.setStatus("r");
        firstorderModel.setOrderId("125411");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(5, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("18, Jan");
        firstorderModel.setDeliveryAddress("7894 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("08:30");
        firstorderModel.setStatus("d");
        firstorderModel.setOrderId("1254112");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(6, firstorderModel);

        firstorderModel = new StaticOrderModel();
        firstorderModel.setDate("18, Jan");
        firstorderModel.setDeliveryAddress("7894 Preston Road, Sutie 200 Plane, Tx 75024");
        firstorderModel.setTime("08:30");
        firstorderModel.setStatus("d");
        firstorderModel.setOrderId("1254");
        firstorderModel.setWareHouseLat("");
        firstorderModel.setWareHouseLong("");
        firstorderModel.setWarehouseName("Guindy");
        firstorderModel.setWareHousePhoneNumber("123456789");
        OrderWholeArrayList.add(7, firstorderModel);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OrderFragment(), "ORDER");
        adapter.addFrag(new ReadyShippingFragment(), "READY FOR SHIPPING");
        adapter.addFrag(new ShippedFragment(), "SHIPPED");
        adapter.addFrag(new DeliveredFragment(), "DELIVERED");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    class FragmentPagerItemExtendsAdapter extends FragmentPagerItemAdapter {

        public FragmentPagerItemExtendsAdapter(FragmentManager fm, FragmentPagerItems pages) {
            super(fm, pages);
        }

        @Override
        public Fragment getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
