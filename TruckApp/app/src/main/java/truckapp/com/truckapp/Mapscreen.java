package truckapp.com.truckapp;

import android.app.AlertDialog;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import truckapp.com.truckapp.model.AddressModel;
import truckapp.com.truckapp.service.location.LatLonFromAddress;
import truckapp.com.truckapp.service.location.OpenActivityListener;
import truckapp.com.truckapp.utils.CommonUtils;
import truckapp.com.truckapp.utils.Constant;

public class Mapscreen extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    ArrayList<AddressModel> addr = new ArrayList<>();
    AlertDialog sportDialog;
    String ZipCode = "";
    String Latitude, Longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapscreen);
        sportDialog = new SpotsDialog(Mapscreen.this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();
        Latitude = bundle.getString("Latitude");
        Longitude = bundle.getString("Longitude");
        ZipCode = bundle.getString("ZipAddress");
        Log.v(Constant.TAG, "ZipCode :- " + ZipCode);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        mMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(bestProvider, 20000, 0, this);

        if (!Latitude.equals("") && !Longitude.equals("")) {

        } else if (!ZipCode.equals("")) {
            LatLonFromAddress latLonFromAddress = new LatLonFromAddress(this, ZipCode, sportDialog, addr, homeScreenOpenActivity, 1);
        } else {
            CommonUtils.ShowToast(this, "Ware Location is not available. Please contact admin.");
        }




       /* // latitude and longitude
        double latitude = 17.385044;
        double longitude = 78.486671;

        // create marker
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Hello Maps");

        // adding marker
        googleMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(17.385044, 78.486671)).zoom(12).build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);*/
    }

    @Override
    public void onLocationChanged(Location location) {
        // latitude and longitude
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        // create marker
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Hello Maps");

        // adding marker
        mMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(latitude, longitude)).zoom(12).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    OpenActivityListener homeScreenOpenActivity = new OpenActivityListener() {
        @Override
        public void OnSuccess(int id, int successId) {
            if (id == 1) {
                /*Intent HomeScreenActivity = new Intent(HomeScreen.this, DimensionScreen.class);
                HomeScreenActivity.putExtra("userAddress", addr.get(successId).getAddress());
                HomeScreenActivity.putExtra("Latitude", String.valueOf(addr.get(successId).getLat()));
                HomeScreenActivity.putExtra("Longitude", String.valueOf(addr.get(successId).getLon()));
                HomeScreenActivity.putExtra("county", String.valueOf(addr.get(successId).getCounty()));
                HomeScreenActivity.putExtra("shortName", addr.get(successId).getShortname());
                HomeScreenActivity.putExtra("dimensionListModel", dimensionListModel);
                startActivity(HomeScreenActivity);*/
                Log.v(Constant.TAG, "Lat :- " + String.valueOf(addr.get(successId)));
                Log.v(Constant.TAG, "Lat :- " + String.valueOf(addr.get(successId).getLat()));
                Log.v(Constant.TAG, "Long :- " + String.valueOf(addr.get(successId).getLon()));
            }
        }

        @Override
        public void OnFailure(String ErrorMessage) {

        }
    };
}
