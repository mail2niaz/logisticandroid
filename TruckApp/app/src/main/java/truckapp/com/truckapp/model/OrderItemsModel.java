package truckapp.com.truckapp.model;

/**
 * Created by Nishanth on 16-06-2016.
 */
public class OrderItemsModel {

    int id;
    int orderId;
    int invMastUid;
    String itemId;
    String itemDesc;
    int quantity;
    String jobNumber;
    int createdAt;
    int updatedAt;
    int lineNum;
    String defaultSellingUnit;

    public String getDefaultSellingUnit() {
        return defaultSellingUnit;
    }

    public void setDefaultSellingUnit(String defaultSellingUnit) {
        this.defaultSellingUnit = defaultSellingUnit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getInvMastUid() {
        return invMastUid;
    }

    public void setInvMastUid(int invMastUid) {
        this.invMastUid = invMastUid;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public int getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(int updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }


}
