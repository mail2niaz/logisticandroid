package truckapp.com.truckapp.model.responseobjects;

import java.io.Serializable;

/**
 * Created by Nishanth on 27-06-2016.
 */
public class orderItemsModel implements Serializable {

    int id;
    int orderId;
    int invMastUid;
    String itemId;
    String itemDesc;
    int quantity;
    String jobNumber;
    String createdAt;
    String updatedAt;
    int lineNum;
    String defaultSellingUnit;
    String itemImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getInvMastUid() {
        return invMastUid;
    }

    public void setInvMastUid(int invMastUid) {
        this.invMastUid = invMastUid;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }

    public String getDefaultSellingUnit() {
        return defaultSellingUnit;
    }

    public void setDefaultSellingUnit(String defaultSellingUnit) {
        this.defaultSellingUnit = defaultSellingUnit;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }
}
