package truckapp.com.truckapp.model.responseobjects.login;

/**
 * Created by EsakkiMuthu on 1/05/2015.
 */
public class UserInfoModel {


    int UserInfoId;

    public int getUserInfoId() {
        return UserInfoId;
    }

    public void setUserInfoId(int userInfoId) {
        UserInfoId = userInfoId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getHomeAddressLine1() {
        return HomeAddressLine1;
    }

    public void setHomeAddressLine1(String homeAddressLine1) {
        HomeAddressLine1 = homeAddressLine1;
    }

    public String getHomeAddressLine2() {
        return HomeAddressLine2;
    }

    public void setHomeAddressLine2(String homeAddressLine2) {
        HomeAddressLine2 = homeAddressLine2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getSocialMediaUserid() {
        return SocialMediaUserid;
    }

    public void setSocialMediaUserid(String socialMediaUserid) {
        SocialMediaUserid = socialMediaUserid;
    }

    public String getSocialMediaType() {
        return SocialMediaType;
    }

    public void setSocialMediaType(String socialMediaType) {
        SocialMediaType = socialMediaType;
    }

    public boolean isBuy() {
        return IsBuy;
    }

    public void setIsBuy(boolean isBuy) {
        IsBuy = isBuy;
    }

    public boolean isRent() {
        return IsRent;
    }

    public void setIsRent(boolean isRent) {
        IsRent = isRent;
    }

    public String getBuyLookingtoClose() {
        return BuyLookingtoClose;
    }

    public void setBuyLookingtoClose(String buyLookingtoClose) {
        BuyLookingtoClose = buyLookingtoClose;
    }

    public String getRentLookingtoClose() {
        return RentLookingtoClose;
    }

    public void setRentLookingtoClose(String rentLookingtoClose) {
        RentLookingtoClose = rentLookingtoClose;
    }

    public String getFirstTimeHomeBuyer() {
        return FirstTimeHomeBuyer;
    }

    public void setFirstTimeHomeBuyer(String firstTimeHomeBuyer) {
        FirstTimeHomeBuyer = firstTimeHomeBuyer;
    }

    public boolean isInvestment() {
        return IsInvestment;
    }

    public void setIsInvestment(boolean isInvestment) {
        IsInvestment = isInvestment;
    }

    public boolean isPrimaryResidence() {
        return IsPrimaryResidence;
    }

    public void setIsPrimaryResidence(boolean isPrimaryResidence) {
        IsPrimaryResidence = isPrimaryResidence;
    }

    String UserName;

    String Password;
    String Email;
    String HomeAddressLine1;
    String HomeAddressLine2;
    String City;
    String State;
    String Zipcode;
    String PhoneNumber;
    String MobileNumber;
    String SocialMediaUserid;
    String SocialMediaType;
    boolean IsBuy;
    boolean IsRent;
    String BuyLookingtoClose;
    String RentLookingtoClose;
    String FirstTimeHomeBuyer;
    boolean IsInvestment;
    boolean IsPrimaryResidence;
}
