package truckapp.com.truckapp.utils;

/**
 * Created by esakkimuthu on 1/12/2016.
 */
public class ErrorMessage {


    public static String no_network = "Please Check network connection";
    public static String offline_ifsc_network = "You are in offline mode,Please enter the below bank details";
    public static String server_busy = "Server busy please try again later";
    public static String please_contact = "Please contact administrator";
    public static String tc_accept = "You must accept to terms and condtions.";


    public static String invalid_pan = "Please check entered PAN";
    public static String enter_pan = "Please enter Pan and try";
    public static String enter_dob = "Please enter Date of birth and try";

    public static String development_inprogress = "Development inprogress";


    // Login Screen
    public static String enter_sap_id = "Please enter User ID and try";
    public static String enter_username = "Please enter username and try";
    public static String enter_password = "Please enter password and try";


    public static String invalid_dob = "Please enter Date of birth and try";
    public static String not_valid_pan = "Individual PAN only accepted.";
    public static String enter_email = "Please enter email id and try.";
    public static String not_valid_email = "Please enter valid email id.";
    public static String enter_mobile = "Please enter mobile number and try.";

    public static String check_mobile = "Please check entered mobile number.";
    public static String check_numeric = "Please check entered number only.";

    public static String not_valid_mail = "Please enter valid mobile numebr.";

    public static String caputre_client_photo = "Please Capture client photo.";

    // Profile Stage Errror Message
    public static String caputre_pan_image = "Please capture PAN photo.";
    public static String enter_first_name = "Please enter first name.";
    public static String select_title = "Please select title.";

    public static String enter_last_name = "Please enter last name.";
    public static String enter_father_name = "Please enter father name.";
    public static String enter_valid_father_name = "Please enter minimum two characters";
    public static String special_characters_not_accepted = "Special characters are not accepted.";
    public static String zeros_not_accepted = "zeros are not accepted.";
    public static String enter_otp = "Please enter OTP.";
    public static String otp_validation = "Please validate OTP .";

    public static String caputre_corresponding_address_image = "Please capture Correspondence Address photo.";
    public static String caputre_permanent_address_image = "Please capture Permanent Address photo.";
    public static String enter_address_line_one = "Please enter address.";
    public static String enter_valid_address_line_one = "Please enter valid address.";
    public static String enter_city = "Please enter city.";
    public static String select_corres_document_proof = "Please select Correspondence address proof type";
    public static String select_perm_document_proof = "Please select Permanent address proof type";
    public static String enter_state = "Please enter state.";
    public static String enter_country = "Please enter country.";
    public static String enter_pincode = "Please enter pincode.";
    public static String address_one_address_format = "Enter vaild alphanumeric and valid special characters";

    public static String select_marital_status = "Please select marital status.";
    public static String select_income_range = "Please select income range.";
    public static String enter_net_worth = "Please enter networth.";
    public static String enter_nother_maiden = "Please enter mother maiden name.";
    public static String enter_valid_mothermaiden_name = "Please enter minimum two characters";
    public static String enter_net_worth_length = "Please enter networth.";
    public static String select_occupation = "Please select occupation.";
    public static String select_education = "Please select education.";
    public static String select_email_belongs = "Please select Email belongs.";
    public static String select_mobile_belongs = "Please select Mobile belongs.";
    public static String enter_other_occupation = "Please enter occupation.";

    public static String invalid_month = "Please check months added";

//Product

    public static String selectProduct = "Please select any one product";
    public static String  selectSegment = "Please select any one segment";
    public static String  selectFO = "Please select FO document type";
//bank
    public static String select_acct_type = "Please select acc type";
    public static String enter_acc = "Please enter acc number";
    public static String enter_minimum_acc = "minimum characters should be 10 character";

    public static String reenter_acc = "Please reenter acc number";

    public static String enter_minimum_reacc = "minimum characters should be 10 character";
    public static String enter_minimum_ifsc= "characters should be 11 character";
    public static String enter_ifsc = "Please enter Ifsc Code";
    public static String check_ifsc = "Please Check Ifsc Code";
    public static String mismatch_acc_reacc = "acc num and reenter acc mismatch";
    public static String select_bank_document_proof = "Please select account type";
    public static String enter_bank_address = "Please enter bank address";
    public static String enter_bank_name = "Please enter bank name";
    public static String enter_micr_code = "Please enter MICR code";

//poa

    public static String enter_nominee = "Please select nominee relationship";
    public static String caputre_signature_image = "Please capture signature photo.";
    public static String caputre_rm_signature_image = "Please capture RM signature photo.";
    public static String caputre_first_page_image = "Please capture first page of POA photo.";
    public static String caputre_first_page_two_image = "Please capture first page of second POA photo.";
    public static String caputre_investment_image = "Please capture investment account photo.";
    public static String caputre_fo_image = "Please capture proof of income photo.";
    public static String caputre_nominee_image = "Please capture nominee photo.";
    public static String caputre_commodity_image = "Please capture Commodity photo.";
    public static String caputre_ecn_image = "Please capture ECN photo.";

    public static String enter_guardian = "Please enter guardian name";
    public static String enter_valid_guardian = "Please enter minimum two chararcters";


    public static String incomplete_registration_msg = "Please complete the registration stage";
    public static String incomplete_profile_msg = "Please complete the profile stage";
    public static String incomplete_product_msg = "Please complete the Product stage";
    public static String incomplete_poa_msg = "Please complete the POA stage";
    public static String incomplete_bank_msg = "Please complete the Bank stage";


    public static String caputre_cheque_image = "Please capture cheque photo.";
    public static String enter_sub_broker = "Provide sub broker name";
    public static String enter_exchange = "Provide Exhange";

    public static String inavalid_gender = "Please select gender";

    public static String special_characters_not_accepted_mobile = "Please enter valid mobile number";

    public static String special_characters_not_accepted_account = "Please enter valid account number";
    //user profile
    public static String bapc_msg = "Please enter BAPC code";
    public static String bapc_incorrect_msg = "Please enter correct BAPC code";
    public static String branch_msg = "Please enter Branch code";
    public static String branch_incorrect_msg = "Please enter correct Branch code";
    public static String atpc_msg = "Please enter ATPC code";
    public static String atpc_incorrect_msg = "Please enter correct ATPC code";
    public static String sales_msg = "Please enter Sales code";
    public static String sales_incorrect_msg = "Please enter correct Sales code";
    public static String omnisys_msg = "Please enter Omnesys code";
    public static String omnisys_incorrect_msg = "Please enter correct Omnesys code";
    public static String group_msg = "Please enter Group code";
    public static String enter_trading_month = "Please enter trading_experience month";
    public static String enter_trading_year = "Please enter trading_experience year";

    public static String edit_msg = "You can update now";

    public static  String IMAGE_ERROR ="Please try again some problem";

    public static String DBINSERT_ISSEUS ="Low internal storage.Please clear the unwanted data";

    public static String VALUES_SAME ="Same addresses are repeated, Please enter proper address.";


    public static String EDIT_ALERT = "Please Edit your Details before submit to DBO.Are You Sure Still you want to Continue?";

    public static String MAPPINGDETAILSERROR="Please update mapping details";
    public static String NONKRACONVERT="Customer is KRA verified but Mandatory details missing. Please update the information.";
}
