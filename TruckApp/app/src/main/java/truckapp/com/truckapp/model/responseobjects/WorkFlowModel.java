package truckapp.com.truckapp.model.responseobjects;

import java.util.List;

/**
 * Created by Nishanth on 23-06-2016.
 */
public class WorkFlowModel {

    public List<TabBarStatusModel> getTabBarStatusModelsArr() {
        return tabBarStatusModelsArr;
    }

    public void setTabBarStatusModelsArr(List<TabBarStatusModel> tabBarStatusModelsArr) {
        this.tabBarStatusModelsArr = tabBarStatusModelsArr;
    }

    List<TabBarStatusModel> tabBarStatusModelsArr;

    /*public ArrayList<TabBarStatusModel> getTabBarStatusModelsArr() {
        return tabBarStatusModelsArr;
    }

    public void setTabBarStatusModelsArr(ArrayList<TabBarStatusModel> tabBarStatusModelsArr) {
        this.tabBarStatusModelsArr = tabBarStatusModelsArr;
    }*/
}
