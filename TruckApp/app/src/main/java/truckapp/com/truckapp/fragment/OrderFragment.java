package truckapp.com.truckapp.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import truckapp.com.truckapp.DetailOrderActivity;
import truckapp.com.truckapp.HomeScreen;
import truckapp.com.truckapp.R;
import truckapp.com.truckapp.model.responseobjects.OrderModel;
import truckapp.com.truckapp.model.responseobjects.submitOrderModel;
import truckapp.com.truckapp.service.servicefactory.ServiceFactory;
import truckapp.com.truckapp.utils.AppController;
import truckapp.com.truckapp.utils.CommonUtils;
import truckapp.com.truckapp.utils.Constant;
import truckapp.com.truckapp.utils.NetworkConnectionDetector;


public class OrderFragment extends Fragment {

    public OrderFragment() {
        // Required empty public constructor
    }

    @Bind(R.id.order_recycler_view)
    RecyclerView order_recycler_view;
    String TAG = "truck";
    ArrayList<OrderModel> orderarrayList = new ArrayList<>();
    ProgressDialog orderProgressDialog;

    private int currentViewTab = 0;
    boolean APICallingStatus = false;
    NetworkConnectionDetector networkConnectionDetector;
    AlertDialog sportDialog;
    HomeScreen homeActivity;
    boolean ackStatus = false;
    String StatusBtnLabel = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, view);
        orderarrayList = new ArrayList<>();


        networkConnectionDetector = new NetworkConnectionDetector(view.getContext());
        homeActivity = (HomeScreen) getActivity();
        sportDialog = homeActivity.sportDialog;
       /* orderProgressDialog = new ProgressDialog(getActivity());
        orderProgressDialog.setMessage("Loading...");
        orderProgressDialog.setCancelable(false);
        if (orderProgressDialog.isShowing()) {
            orderProgressDialog.dismiss();
        }*/

        if (HomeScreen.currentSelectedTab == FragmentPagerItem.getPosition(getArguments())) {
            currentViewTab = FragmentPagerItem.getPosition(getArguments());
            ackStatus = HomeScreen.myList.get(currentViewTab).isItemAck();
            StatusBtnLabel = HomeScreen.myList.get(currentViewTab).getStatusLabel();
            //CommonUtils.ShowToast(getActivity(), "Fragment if " + HomeScreen.myList.get(currentViewTab).getStatusId());
           /* for (int i = 0; i < HomeScreen.OrderWholeArrayList.size(); i++) {
                Log.v(TAG, "Status :- " + HomeScreen.OrderWholeArrayList.get(i).getStatus());
                if ((HomeScreen.OrderWholeArrayList.get(i).getStatus()).equalsIgnoreCase("o")) {
                    Log.v(TAG, "insterted");
                    orderarrayList.add(HomeScreen.OrderWholeArrayList.get(i));
                }
            }
            Log.v(TAG, "Size :- " + orderarrayList.size());

            order_recycler_view.setHasFixedSize(true);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            order_recycler_view.setLayoutManager(mLayoutManager);

            OrderAdapter dimension_adapter = new OrderAdapter();
            order_recycler_view.setAdapter(dimension_adapter);
            dimension_adapter.notifyDataSetChanged();*/
            Log.v(Constant.TAG, "Truck ID :- " + AppController.getInstance().truckId);
            Log.v(Constant.TAG, "Truck getStatusId :- " + HomeScreen.myList.get(currentViewTab).getStatusId());
            if (networkConnectionDetector.connectionStatus()) {
                if (!APICallingStatus) {
                    if (sportDialog.isShowing()) {

                    } else {
                        sportDialog.show();
                    }
                    collectCurrentStatusList(AppController.getInstance().truckId, HomeScreen.myList.get(currentViewTab).getStatusId());
                }
            } else {
                CommonUtils.ShowToast(getActivity(), "Network Connection is not response properly. Please check your connection.");
            }
        } else {
            CommonUtils.Log(getActivity(), "Fragment " + currentViewTab);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (HomeScreen.currentSelectedTab == FragmentPagerItem.getPosition(getArguments())) {
            Log.v(Constant.TAG, "Resume Truck ID :- " + AppController.getInstance().truckId);
            Log.v(Constant.TAG, "Resume Truck StatusId :- " + HomeScreen.myList.get(currentViewTab).getStatusId());
            if (networkConnectionDetector.connectionStatus()) {
                if (!APICallingStatus) {
                    if (sportDialog.isShowing()) {

                    } else {
                        sportDialog.show();
                    }
                    collectCurrentStatusList(AppController.getInstance().truckId, HomeScreen.myList.get(currentViewTab).getStatusId());
                }
            } else {
                CommonUtils.ShowToast(getActivity(), "Network Connection is not response properly. Please check your connection.");
            }
        }
    }

    private void collectCurrentStatusList(String truckId, String position) {
        APICallingStatus = true;


        ServiceFactory.get().GetOrderList(truckId, position, new Callback<List<OrderModel>>() {
            @Override
            public void success(List<OrderModel> orderModelList, Response response) {
                // success!

                // sportDialog.dismiss();
                //   orderProgressDialog.dismiss();
                orderarrayList.clear();
                APICallingStatus = false;
                createOrderList(orderModelList);


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong

                sportDialog.dismiss();
                APICallingStatus = false;
                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                // mLogin.convertViewModel(loginModel);


            }
        });

    }

    private void createOrderList(List<OrderModel> orderModelList) {

        Log.v(Constant.TAG, " Size --->" + orderModelList.size());
        if (orderModelList.size() > 0) {

            for (int i = 0; i < orderModelList.size(); i++) {
                Log.v(TAG, "Status :- " + orderModelList.get(i).getId());
                orderarrayList.add(orderModelList.get(i));

            }
            Log.v(TAG, "Size :- " + orderarrayList.size());

            order_recycler_view.setHasFixedSize(true);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            order_recycler_view.setLayoutManager(mLayoutManager);

            OrderAdapter dimension_adapter = new OrderAdapter();
            order_recycler_view.setAdapter(dimension_adapter);
            dimension_adapter.notifyDataSetChanged();
            sportDialog.dismiss();
        } else {
            sportDialog.dismiss();
            CommonUtils.ShowToast(getActivity(), "You dont have any order in this list.");
        }
    }

   /* @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }*/


    private void openDetailsOrderActvity(OrderModel selectedOrderModel) {

        Intent ShareAddressToPalsScreenActivity = new Intent(getActivity(), DetailOrderActivity.class);
        //ShareAddressToPalsScreenActivity.putExtra("CurrentViewHistoryModel", v_h_Model);
        Log.v(Constant.TAG, "Order Id outside :- " + selectedOrderModel.getId());

        ShareAddressToPalsScreenActivity.putExtra("OrderId", String.valueOf(selectedOrderModel.getId()));
        ShareAddressToPalsScreenActivity.putExtra("date", selectedOrderModel.getOrderDate());
        ShareAddressToPalsScreenActivity.putExtra("toStatus", HomeScreen.myList.get(currentViewTab).getToStatusId());

        if (!CommonUtils.cutNull(selectedOrderModel.getOrderAddresses()).equals("")) {
            Log.v(Constant.TAG, "Zip Code :- " + selectedOrderModel.getOrderAddresses().getZip());

            if (CommonUtils.cutNull(selectedOrderModel.getOrderAddresses().getZip()).equals("")) {
                ShareAddressToPalsScreenActivity.putExtra("ZipAddress", selectedOrderModel.getOrderAddresses().getZip());
                ShareAddressToPalsScreenActivity.putExtra("Latitude", "");
                ShareAddressToPalsScreenActivity.putExtra("Longitude", "");
            } else {
                ShareAddressToPalsScreenActivity.putExtra("ZipAddress", "");
                ShareAddressToPalsScreenActivity.putExtra("Latitude", "");
                ShareAddressToPalsScreenActivity.putExtra("Longitude", "");
            }

            if (!CommonUtils.cutNull(selectedOrderModel.getOrderAddresses()).equals("")) {
                ShareAddressToPalsScreenActivity.putExtra("deliveraddress", selectedOrderModel.getOrderAddresses().getAddress());
            } else {
                ShareAddressToPalsScreenActivity.putExtra("deliveraddress", "Please Contact Ware house.");
            }
        } else {
            ShareAddressToPalsScreenActivity.putExtra("ZipAddress", "");
            ShareAddressToPalsScreenActivity.putExtra("Latitude", "");
            ShareAddressToPalsScreenActivity.putExtra("Longitude", "");
            ShareAddressToPalsScreenActivity.putExtra("deliveraddress", "Please Contact Ware house.");
        }
        if (selectedOrderModel.getOrderItems().size() > 0) {
            ShareAddressToPalsScreenActivity.putExtra("orderItemStatus", "present");
            ShareAddressToPalsScreenActivity.putExtra("orderItems", (Serializable) selectedOrderModel.getOrderItems());
        } else {
            ShareAddressToPalsScreenActivity.putExtra("orderItemStatus", "");
        }
        //ShareAddressToPalsScreenActivity.putExtra("currentSelectedOrderModel", selectedOrderModel);

        startActivity(ShareAddressToPalsScreenActivity);
        //startActivity(new Intent(getActivity(), DetailOrderActivity.class));
    }


    public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

        public OrderAdapter() {
            super();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_item_other, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int postion) {

            Log.v(Constant.TAG, postion + " OrderId :- " + orderarrayList.get(postion).getId());
            viewHolder.order_id_number_txt.setText(String.valueOf(orderarrayList.get(postion).getId()));
            Log.v(Constant.TAG, postion + " WareHouse :- " + orderarrayList.get(postion).getLocations());
            viewHolder.ware_house_name_txt.setText(orderarrayList.get(postion).getLocations());

            if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderAddresses()).equals("")) {
                if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderAddresses().getAddress()).equals("")) {
                    Log.v(Constant.TAG, postion + " DeliveryAddress :- " + orderarrayList.get(postion).getOrderAddresses().getAddress());
                    viewHolder.address_txt.setText(orderarrayList.get(postion).getOrderAddresses().getAddress());
                }
            }

            if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderDate()).equals("")) {
                viewHolder.card_order_time.setText(orderarrayList.get(postion).getOrderDate());
            }
            Log.v(Constant.TAG, "StatusBtnLabel :- " + StatusBtnLabel);
            if (!StatusBtnLabel.equals("")) {
                viewHolder.Status_txt.setText(StatusBtnLabel.toString());
            } else {
                viewHolder.Status_txt.setVisibility(View.INVISIBLE);
            }
//            viewHolder.card_order_date.setText(orderarrayList.get(postion).getDate());

            if (ackStatus) {
                viewHolder.card_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDetailsOrderActvity(orderarrayList.get(postion));
                    }
                });
            } else {
                viewHolder.card_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //openDetailsOrderActvity(orderarrayList.get(postion), viewHolder.card_container);
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        if (!CommonUtils.cutNull(HomeScreen.myList.get(currentViewTab).getToStatusId()).equals("")) {
                                            getConformationProcess(orderarrayList.get(postion).getId(), HomeScreen.myList.get(currentViewTab).getToStatusId());
                                        } else {
                                            CommonUtils.ShowToast(homeActivity, "Order Status is missing.Please contact Administrator.");
                                        }
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:

                                        break;
                                }
                            }
                        };


                        AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
                        builder.setMessage("Would you like to process the order to next step ?").setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                    }
                });
            }

          /*  viewHolder.history_fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });
            viewHolder.history_notes_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            viewHolder.history_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/

        }

        @Override
        public int getItemCount() {
            return orderarrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public TextView order_id_number_txt, Status_txt;
            public TextView ware_house_name_txt, address_txt, card_order_time, card_order_date;
            LinearLayout card_container;

            public ViewHolder(View itemView) {
                super(itemView);
                this.order_id_number_txt = (TextView) itemView.findViewById(R.id.card_order_id_number);
                this.ware_house_name_txt = (TextView) itemView.findViewById(R.id.card_ware_house_name);
                this.address_txt = (TextView) itemView.findViewById(R.id.card_address_txt);
                this.card_order_time = (TextView) itemView.findViewById(R.id.card_order_time);
                this.Status_txt = (TextView) itemView.findViewById(R.id.card_status_txt);
                this.card_container = (LinearLayout) itemView.findViewById(R.id.card_whole_container_linear);
            }
        }
    }

    private void getConformationProcess(int orderId, String toStatusId) {

        Log.v(Constant.TAG, "Status Id: - " + String.valueOf(orderId));
        Log.v(Constant.TAG, "toStatusId :- " + toStatusId);
        ServiceFactory.get().submitOrder(String.valueOf(orderId), toStatusId, new Callback<submitOrderModel>() {
            @Override
            public void success(submitOrderModel loginModel, Response response) {
                // success!
                Log.v(Constant.TAG, "--->" + loginModel.getId());
            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                // mLogin.convertViewModel(loginModel);


            }
        });
    }

}
