package truckapp.com.truckapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import truckapp.com.truckapp.HomeScreen;
import truckapp.com.truckapp.R;
import truckapp.com.truckapp.model.StaticOrderModel;


public class DeliveredFragment extends Fragment {

    public DeliveredFragment() {
        // Required empty public constructor
    }

    @Bind(R.id.delivered_recycler_view)
    RecyclerView ready_shipping_recycler_view;
    String TAG = "truck";
    ArrayList<StaticOrderModel> deliveredarrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delivered, container, false);

        ButterKnife.bind(this, view);
        deliveredarrayList = new ArrayList<>();
        for (int i = 0; i < HomeScreen.OrderWholeArrayList.size(); i++) {
            Log.v(TAG, "Status :- " + HomeScreen.OrderWholeArrayList.get(i).getStatus());
            if ((HomeScreen.OrderWholeArrayList.get(i).getStatus()).equalsIgnoreCase("d")) {
                Log.v(TAG, "ready for shipping insterted");
                deliveredarrayList.add(HomeScreen.OrderWholeArrayList.get(i));
            }
        }
        Log.v(TAG, "Size :- " + deliveredarrayList.size());

        ready_shipping_recycler_view.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        ready_shipping_recycler_view.setLayoutManager(mLayoutManager);

        ReadyShippingAdapter dimension_adapter = new ReadyShippingAdapter();
        ready_shipping_recycler_view.setAdapter(dimension_adapter);
        dimension_adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        recycleDeliveredList();
    }

    private void recycleDeliveredList() {
        deliveredarrayList = new ArrayList<>();
        for (int i = 0; i < HomeScreen.OrderWholeArrayList.size(); i++) {
            Log.v(TAG, "Status :- " + HomeScreen.OrderWholeArrayList.get(i).getStatus());
            if ((HomeScreen.OrderWholeArrayList.get(i).getStatus()).equalsIgnoreCase("d")) {
                Log.v(TAG, "ready for shipping insterted");
                deliveredarrayList.add(HomeScreen.OrderWholeArrayList.get(i));
            }
        }
        Log.v(TAG, "Size :- " + deliveredarrayList.size());

        ready_shipping_recycler_view.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        ready_shipping_recycler_view.setLayoutManager(mLayoutManager);

        ReadyShippingAdapter dimension_adapter = new ReadyShippingAdapter();
        ready_shipping_recycler_view.setAdapter(dimension_adapter);
        dimension_adapter.notifyDataSetChanged();
    }

    public class ReadyShippingAdapter extends RecyclerView.Adapter<ReadyShippingAdapter.ViewHolder> {

        public ReadyShippingAdapter() {
            super();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_item_other, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }


        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int postion) {

            Log.v(TAG, postion + " OrderId :- " + deliveredarrayList.get(postion).getOrderId());
            viewHolder.order_id_number_txt.setText(deliveredarrayList.get(postion).getOrderId());
            Log.v(TAG, postion + " WareHouse :- " + deliveredarrayList.get(postion).getWarehouseName());
            viewHolder.ware_house_name_txt.setText(deliveredarrayList.get(postion).getWarehouseName());
            Log.v(TAG, postion + " DeliveryAddress :- " + deliveredarrayList.get(postion).getDeliveryAddress());
            viewHolder.address_txt.setText(deliveredarrayList.get(postion).getDeliveryAddress());
            viewHolder.card_order_time.setText(deliveredarrayList.get(postion).getTime());

            viewHolder.card_status_txt.setText("Delivered");
            viewHolder.card_status_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*HomeScreen.OrderWholeArrayList.get(postion).setStatus("s");
                    Log.v(TAG, "Current Status :- " + HomeScreen.OrderWholeArrayList.get(postion).getStatus());*/


                    Log.v(TAG, "Current orderId :- " + deliveredarrayList.get(postion).getOrderId());
                    for (int i = 0; i < HomeScreen.OrderWholeArrayList.size(); i++) {
                        Log.v(TAG, "Order Id - " + HomeScreen.OrderWholeArrayList.get(i).getOrderId());
                        if (deliveredarrayList.get(postion).getOrderId().equals(HomeScreen.OrderWholeArrayList.get(i).getOrderId())) {
                            Log.v(TAG, "status Changed");
                            HomeScreen.OrderWholeArrayList.get(i).setStatus("o");
                            Log.v(TAG, "Current Status :- " + HomeScreen.OrderWholeArrayList.get(i).getStatus());
                        }
                    }
                    recycleDeliveredList();
                }
            });
//            viewHolder.card_order_date.setText(deliveredarrayList.get(postion).getDate());

            /*viewHolder.history_refresh_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            viewHolder.history_fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });
            viewHolder.history_notes_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            viewHolder.history_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/

        }

        @Override
        public int getItemCount() {
            return deliveredarrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public TextView order_id_number_txt;
            public TextView ware_house_name_txt, address_txt, card_order_time, card_order_date, card_status_txt;
            /*public ImageView history_fav_img, history_notes_img, history_share_img, history_refresh_img;
            public android.support.v7.widget.CardView card_view;*/


            public ViewHolder(View itemView) {
                super(itemView);
                this.order_id_number_txt = (TextView) itemView.findViewById(R.id.card_order_id_number);
                this.ware_house_name_txt = (TextView) itemView.findViewById(R.id.card_ware_house_name);
                this.address_txt = (TextView) itemView.findViewById(R.id.card_address_txt);
                this.card_order_time = (TextView) itemView.findViewById(R.id.card_order_time);
                this.card_status_txt = (TextView) itemView.findViewById(R.id.card_status_txt);
                //this.card_order_date = (TextView) itemView.findViewById(R.id.card_order_date);
            }
        }
    }

}
