package truckapp.com.truckapp.model.responseobjects.login;

/**
 * Created by Nishanth on 23-06-2016.
 */
public class CustomersModel {

    int id;
    String name;
    String address;
    String city;
    String state;
    String zipCode;
    String phone;
    int shipToId;
    int defaultBranchId;
    String createdAt;
    String updatedAt;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getShipToId() {
        return shipToId;
    }

    public void setShipToId(int shipToId) {
        this.shipToId = shipToId;
    }

    public int getDefaultBranchId() {
        return defaultBranchId;
    }

    public void setDefaultBranchId(int defaultBranchId) {
        this.defaultBranchId = defaultBranchId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
