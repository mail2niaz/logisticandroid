package truckapp.com.truckapp.model.responseobjects;

import java.io.Serializable;

/**
 * Created by Nishanth on 23-06-2016.
 */
public class TabBarStatusModel implements Serializable{


    int id;
    String statusDesc;
    String statusId;
    String toStatusId;
    boolean itemAck;
    String roleAccess;
    String createdAt;
    String updatedAt;
    String statusLabel;
    int statusSeq;
    String helpDoc;
    boolean attachDriver;


    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public int getStatusSeq() {
        return statusSeq;
    }

    public void setStatusSeq(int statusSeq) {
        this.statusSeq = statusSeq;
    }

    public String getHelpDoc() {
        return helpDoc;
    }

    public void setHelpDoc(String helpDoc) {
        this.helpDoc = helpDoc;
    }

    public boolean isAttachDriver() {
        return attachDriver;
    }

    public void setAttachDriver(boolean attachDriver) {
        this.attachDriver = attachDriver;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getToStatusId() {
        return toStatusId;
    }

    public void setToStatusId(String toStatusId) {
        this.toStatusId = toStatusId;
    }

    public boolean isItemAck() {
        return itemAck;
    }

    public void setItemAck(boolean itemAck) {
        this.itemAck = itemAck;
    }

    public String getRoleAccess() {
        return roleAccess;
    }

    public void setRoleAccess(String roleAccess) {
        this.roleAccess = roleAccess;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
