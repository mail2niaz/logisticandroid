package truckapp.com.truckapp.model.responseobjects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nishanth on 27-06-2016.
 */
public class OrderModel implements Serializable {


    int id;
    int customerId;
    int userId;
    int locationId;
    String poNumber;
    int truckIdl;
    String orderDate;
    String orderDueDate;
    String createdAt;
    String updatedAt;
    String orderType;
    String orderStatus;
    boolean exported;
    String orderNotes;

    orderAddressesModel orderAddresses;
    List<orderItemsModel> orderItems;
    String locations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public int getTruckIdl() {
        return truckIdl;
    }

    public void setTruckIdl(int truckIdl) {
        this.truckIdl = truckIdl;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderDueDate() {
        return orderDueDate;
    }

    public void setOrderDueDate(String orderDueDate) {
        this.orderDueDate = orderDueDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isExported() {
        return exported;
    }

    public void setExported(boolean exported) {
        this.exported = exported;
    }

    public String getOrderNotes() {
        return orderNotes;
    }

    public void setOrderNotes(String orderNotes) {
        this.orderNotes = orderNotes;
    }

    public orderAddressesModel getOrderAddresses() {
        return orderAddresses;
    }

    public void setOrderAddresses(orderAddressesModel orderAddresses) {
        this.orderAddresses = orderAddresses;
    }

    public List<orderItemsModel> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<orderItemsModel> orderItems) {
        this.orderItems = orderItems;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }
}
