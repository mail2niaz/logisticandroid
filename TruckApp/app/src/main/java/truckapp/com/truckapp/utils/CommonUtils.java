package truckapp.com.truckapp.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Nishanth on 10-01-2016.
 */
public class CommonUtils {

    /**
     * @param mContext
     * @param message
     * @return
     */
    public static ProgressDialog getProgressDialog(Context mContext, String message) {

        ProgressDialog mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);

        mProgressDialog.setCanceledOnTouchOutside(false);
        return mProgressDialog;
    }

    /**
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }


    /**
     * @param imagePath
     * @return
     */
    public static String convertToBase64(String imagePath) {
//        Bitmap bm = BitmapFactory.decodeFile(imagePath);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
//        byte[] byteArrayImage = baos.toByteArray();
//        Log.i(TAG, "Base64: "+Base64.encodeToString(byteArrayImage, Base64.DEFAULT));
//        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        byte[] bytes;
        try {
            FileInputStream inputStream = new FileInputStream(imagePath);
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
            return Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }


    /**
     * @param mContext
     * @return
     */
    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.v("Network State :- ", info[i].getTypeName());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * @param mContext
     */
    public static void logoutAction(final Context mContext) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                       /* AppPreference.getInstance(mContext).putString(
                                AppPreference.StringKeys.USERID, "false");
                        AppPreference.getInstance(mContext).putString(
                                AppPreference.StringKeys.QUESTIONFLAG, "false");

                        DatabaseHandler db=new DatabaseHandler(mContext);
                        db.removeAddressData();*/

                        LogOutApplication(mContext);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Would you like to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    /**
     *
     */
    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    /**
     * @param mContext
     */
    public static void LogOutApplication(final Context mContext) {
       /* Intent intent = new Intent(mContext.getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);*/
    }


    /**
     * @param format
     * @return
     */
    public static String cutNull(Object format) {


        String cut;

        if (format == null) {
            cut = "";
        } else if (format.toString().trim().equalsIgnoreCase("")) {
            cut = "";
        } else if (format.toString().trim().equalsIgnoreCase("null")) {
            cut = "";
        } else {

            cut = format.toString();
        }
        return cut;
    }


    /**
     * @param mContext
     * @param message
     */
    public static void Log(Context mContext, String message) {
        Log.v(Constant.TAG, message);
    }

    public static void ShowToast(Context mContext, String message) {
        Toast.makeText(mContext, message,
                Toast.LENGTH_SHORT).show();
    }

    /**
     * @return
     */
    public static ArrayList<String> getMaritalStatus() {
        ArrayList<String> marital_ar = new ArrayList<>();
        marital_ar.add("Single");
        marital_ar.add("Married");
        marital_ar.add("Divorced");
        marital_ar.add("Widow");
        marital_ar.add("Widower");
        marital_ar.add("Separate");

        return marital_ar;
    }


    /**
     * @param baos
     * @return
     */
    public static String convertByteArrayToBase64(ByteArrayOutputStream baos) {
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    /**
     * @param base64Str
     * @return
     */
    public static Bitmap convertBase64ToBitmap(String base64Str) {

        byte[] decodedString = Base64.decode(base64Str, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

    }


    public static void setSwitchBackground(SwitchCompat switchCompat) {


        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_checked},
                new int[]{android.R.attr.state_checked},
        };

        int[] thumbColors = new int[]{
                Color.WHITE,
                Color.WHITE,
        };

        int[] trackColors = new int[]{
                Color.RED,
                Color.GREEN,
        };


        DrawableCompat.setTintList(DrawableCompat.wrap(switchCompat.getThumbDrawable()), new ColorStateList(states, thumbColors));
        DrawableCompat.setTintList(DrawableCompat.wrap(switchCompat.getTrackDrawable()), new ColorStateList(states, trackColors));


    }
}