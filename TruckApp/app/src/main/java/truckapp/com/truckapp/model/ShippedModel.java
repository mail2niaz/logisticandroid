package truckapp.com.truckapp.model;

/**
 * Created by Nishanth on 15-06-2016.
 */
public class ShippedModel {

    String Date;
    String Time;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getWarehouseName() {
        return WarehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        WarehouseName = warehouseName;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getWareHousePhoneNumber() {
        return WareHousePhoneNumber;
    }

    public void setWareHousePhoneNumber(String wareHousePhoneNumber) {
        WareHousePhoneNumber = wareHousePhoneNumber;
    }

    public String getWareHouseLat() {
        return WareHouseLat;
    }

    public void setWareHouseLat(String wareHouseLat) {
        WareHouseLat = wareHouseLat;
    }

    public String getWareHouseLong() {
        return WareHouseLong;
    }

    public void setWareHouseLong(String wareHouseLong) {
        WareHouseLong = wareHouseLong;
    }

    String OrderId;
    String WarehouseName;
    String DeliveryAddress;
    String WareHousePhoneNumber;
    String WareHouseLat;
    String WareHouseLong;

}
