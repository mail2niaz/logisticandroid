package truckapp.com.truckapp.model.responseobjects;

import truckapp.com.truckapp.model.responseobjects.login.CustomersModel;
import truckapp.com.truckapp.model.responseobjects.login.TrucksModel;
import truckapp.com.truckapp.model.responseobjects.login.UsersModel;

/**
 * Created by Nishanth on 24-09-2015.
 */
public class LoginModel {

    TrucksModel trucks;
    UsersModel users;
    CustomersModel customers;

    public TrucksModel getTrucks() {
        return trucks;
    }

    public void setTrucks(TrucksModel trucks) {
        this.trucks = trucks;
    }

    public UsersModel getUsers() {
        return users;
    }

    public void setUsers(UsersModel users) {
        this.users = users;
    }

    public CustomersModel getCustomers() {
        return customers;
    }

    public void setCustomers(CustomersModel customers) {
        this.customers = customers;
    }
}