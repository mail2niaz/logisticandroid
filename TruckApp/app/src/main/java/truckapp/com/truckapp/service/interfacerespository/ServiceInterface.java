package truckapp.com.truckapp.service.interfacerespository;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import truckapp.com.truckapp.model.responseobjects.LoginModel;
import truckapp.com.truckapp.model.responseobjects.OrderModel;
import truckapp.com.truckapp.model.responseobjects.TabBarStatusModel;
import truckapp.com.truckapp.model.responseobjects.submitOrderModel;


/**
 * Created by Ramya on 01/05/2016.
 */


public interface ServiceInterface {

    //*********************************LOGIN START*********************************************

    /**
     * LOGIN
     *
     * @param response
     */

    /*@FormUrlEncoded
    @POST("/api/User/Login")
    public void Login(@Field("username") String username, @Field("password") String password, Callback<LoginModel> response);*/


    /*@Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/Login/Authentication")
    public void Login(@Body LoginRequest username, Callback<LoginModel> response);*/
    //@Headers({"Content-Type: application/json;charset=UTF-8"})
    @FormUrlEncoded
    @POST("/login_truck")
    public void Login(@Field("truckId") String email, @Field("password") String password, Callback<LoginModel> response);


    @FormUrlEncoded
    @POST("/order_workflow")
    public void OrderWorkFlow(@Field("role") String role, Callback<List<TabBarStatusModel>> response);


    @FormUrlEncoded
    @POST("/orders_truck")
    public void GetOrderList(@Field("truckId") String truckId, @Field("status") String status, Callback<List<OrderModel>> response);


    @FormUrlEncoded
    @POST("/update_order")
    public void submitOrder(@Field("order_id") String truckId, @Field("order_status") String status, Callback<submitOrderModel> response);
}