package warehousemanagement.com.warehousemanagement.model.requestobjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nishanth on 21-06-2016.
 */
public class LoginRequestModel {

    @SerializedName("truckId")
    int truckId;

    @SerializedName("password")
    String password;

    public LoginRequestModel(int id, String password) {
        this.truckId = id;
        this.password = password;
    }
}
