package warehousemanagement.com.warehousemanagement.service.location;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import warehousemanagement.com.warehousemanagement.model.AddressModel;
import warehousemanagement.com.warehousemanagement.utils.CommonUtils;
import warehousemanagement.com.warehousemanagement.utils.Constant;



/**
 * Created by Nishanth on 19-10-2015.
 */
public class LatLonFromAddress {

    Context mContext;
    String enterTextValue;
    //    CustomAutoCompleteTextView textView;
    AlertDialog sportDialog;
    ArrayList<AddressModel> addr;
    //DimensionListModel dimensionListModel;
    OpenActivityListener openActivityListener;
    int id;

    // Publish Key
    // homitatkey@gmail.com
    // Project name :- homitat live autocomplete1,homitat live autocomplete2,homitat live autocomplete3,homitat live autocomplete4 --
    String[] autoCompleteKey = {"AIzaSyAy3cfKisG_28CG48TCBkaVzvPPGhZoUqI", "AIzaSyDTX1o4Af9OuMzN3-ojatfu81WfAPnxmKA", "AIzaSyC4As6z1gNdU_mz8rc7tAWhDKHXfapdFlE", "AIzaSyCqEhBqcsO9Kkmj83Ahk3QhzW_bOi4oNlI"};


    public LatLonFromAddress(Context mContext, String enterTextValue, AlertDialog sportDialog, ArrayList<AddressModel> addr, OpenActivityListener openActivityListener, int id) {

        this.mContext = mContext;
        this.enterTextValue = enterTextValue;
        //this.textView = textView;
        this.sportDialog = sportDialog;
        this.addr = addr;
        this.openActivityListener = openActivityListener;
        this.id = id;

        new LongOperation().execute(enterTextValue);

    }


    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String data = "";
            try {

                String url = "https://maps.google.com/maps/api/geocode/json?address=" + params[0] + "&key=" + Constant.geoCodeKey + "&sensor=false".replace(" ", "%20");
                url = url.replaceAll(" ", "%20");
                System.out.println("resulturl" + "resulturl :- " + url);

                data = downloadUrl(url);
            } catch (IOException e) {
                e.printStackTrace();
                data = "";
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
//            LoadingIndicatior.hide();
            sportDialog.dismiss();
            System.out.println("resultlocation" + "resultlocation " + result);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(result);
                getLatLong(jsonObject);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            addr.clear();

//            LoadingIndicatior.show();
            sportDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    public boolean getLatLong(JSONObject jsonObject) {

        addr.clear();
        boolean stateflag = false;
        String short_address = "", county_address = "";
        try {

            JSONArray jsv = ((JSONArray) jsonObject.get("results"));
            for (int i = 0; i < jsv.length(); i++) {
                stateflag = true;
                JSONObject jobj = jsv.getJSONObject(i)
                        .getJSONObject("geometry");

                AddressModel addrmodel = new AddressModel();
                double longitute = jobj.getJSONObject("location")
                        .getDouble("lng");

                double latitude = jobj.getJSONObject("location")
                        .getDouble("lat");


                String checkformatted_address = jsv.getJSONObject(i)
                        .getString("formatted_address");


                JSONArray js = jsv.getJSONObject(i)
                        .getJSONArray("address_components");
                int value = js.length();

                for (int k = 0; k < js.length(); k++) {

                    if (js.getJSONObject(k).getJSONArray("types").length() == 2) {
                        String types_str_one = js.getJSONObject(k).getJSONArray("types").get(0).toString();
                        String types_str_two = js.getJSONObject(k).getJSONArray("types").get(1).toString();
                        Log.v("", "checkformatted_addressshort_address types_str_one:- " + types_str_one);
                        Log.v("", "checkformatted_addressshort_address types_str_two:- " + types_str_two);
                        if (types_str_one.equals("administrative_area_level_1") && types_str_two.equalsIgnoreCase("political")) {
                            short_address = js.getJSONObject(k).getString("short_name");
                            Log.v("", "checkformatted_addressshort_address true:- ");
                        }
                        if (types_str_one.equals("administrative_area_level_2") && types_str_two.equalsIgnoreCase("political")) {
                            county_address = js.getJSONObject(k).getString("short_name").trim();
                            Log.v("", "county_address true:- " + county_address);
                        }
                    }

                }


                Log.v("", "checkformatted_addressshort_address :- " + short_address);
                Log.v("", "checklat" + String.valueOf(longitute));
                addrmodel.setLat(latitude);
                addrmodel.setLon(longitute);
                addrmodel.setIsPrimary("false");
                addrmodel.setState("");
                addrmodel.setShortname(short_address);
                addrmodel.setCounty(county_address);

                if (js.length() == 1) {
                    //addrmodel.setAddress(textView.getText().toString());
                } else {
                    addrmodel.setAddress(checkformatted_address);
                }

                addr.add(addrmodel);
              /*  Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());


                addresses = geocoder.getFromLocation(latitude, longitute, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                String state = addresses.get(0).getAdminArea();

                Log.v("", "checkformatted_addressstate" + state);


                if (state != null) {
                    if (!state.equalsIgnoreCase("null") || !state.equals("")) {

                        addrmodel.setLat(latitude);
                        addrmodel.setLon(longitute);
                        addrmodel.setState(state);
                        addrmodel.setShortname(short_address);
                        addrmodel.setAddress(checkformatted_address);
                        addr.add(addrmodel);
                        stateflag=true;

                    } else {
                        //CommonUtils.ShowToast(mContext, "Please enter correct address");
                    }
                } else {


                    //CommonUtils.ShowToast(mContext, "Please enter correct address");
                }*/


            }

            if (stateflag) {

                if (jsv.length() == 1) {
                    //openDemensionAction();
                    // textView.setText(addr.get(0).getAddress());
                   /* Intent HomeScreenActivity = new Intent(mContext, DimensionScreen.class);
                    HomeScreenActivity.putExtra("userAddress", textView.getText().toString());
                    HomeScreenActivity.putExtra("Latitude", String.valueOf(addr.get(0).getLat()));
                    HomeScreenActivity.putExtra("Longitude", String.valueOf(addr.get(0).getLon()));
                    HomeScreenActivity.putExtra("shortName", addr.get(0).getShortname());
                    HomeScreenActivity.putExtra("dimensionListModel", dimensionListModel);
                    mContext.startActivity(HomeScreenActivity);*/

                    openActivityListener.OnSuccess(id, 0);

                } else {
                    alertOpen();
                }


            } else {
                CommonUtils.ShowToast(mContext, "Please enter correct address");
            }

        } catch (JSONException e) {


            CommonUtils.ShowToast(mContext, "Please enter correct address");
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.ShowToast(mContext, "Please enter correct address");
            return false;
        }

        return true;
    }

    private void alertOpen() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        // builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Please Select your address:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                mContext,
                android.R.layout.select_dialog_singlechoice);

        for (int i = 0; i < addr.size(); i++) {
            AddressModel addm = addr.get(i);
            arrayAdapter.add(addm.getAddress());

        }

        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                       /* AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                mContext);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected address is");
                        builderInner.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                        openDemensionAction();
                                    }
                                });
                        builderInner.show();*/
                        CommonUtils.ShowToast(mContext, "Address 0 :- " + addr.get(0).getAddress());
                        // textView.setText(addr.get(0).getAddress());
                        /*Intent HomeScreenActivity = new Intent(mContext, DimensionScreen.class);
                        HomeScreenActivity.putExtra("userAddress", addr.get(which).getAddress());
                        HomeScreenActivity.putExtra("Latitude", String.valueOf(addr.get(which).getLat()));
                        HomeScreenActivity.putExtra("Longitude", String.valueOf(addr.get(which).getLon()));
                        HomeScreenActivity.putExtra("shortName", addr.get(which).getShortname());
                        HomeScreenActivity.putExtra("dimensionListModel", dimensionListModel);
                        mContext.startActivity(HomeScreenActivity);*/
                        openActivityListener.OnSuccess(id, which);
                    }
                });
        builderSingle.show();
    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        try {

            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url
                urlConnection.connect();

                // Reading data from url
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb = new StringBuffer();

                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                data = sb.toString();

                br.close();

            } catch (Exception e) {
                //Log.d("Exception while downloading url", e.toString());
                return "";
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        } catch (Exception e) {
            data = "";

            return data;
        }

    }
}
