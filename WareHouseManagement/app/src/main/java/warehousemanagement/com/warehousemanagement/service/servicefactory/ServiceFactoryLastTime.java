package warehousemanagement.com.warehousemanagement.service.servicefactory;


import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import warehousemanagement.com.warehousemanagement.service.interfacerespository.ServiceInterface;
import warehousemanagement.com.warehousemanagement.utils.AppController;
import warehousemanagement.com.warehousemanagement.utils.Constant;


/**
 * Created by Ramya on 01/05/2016.
 */
public class ServiceFactoryLastTime {

    long timer;

    private static ServiceInterface REST_CLIENT;
    private static String ROOT = Constant.SERVER_URL;
    private static  OkHttpClient httpClient;

    /**
     * to setup the restclient method
     */
    static {
        setupRestClient();
    }

    private ServiceFactoryLastTime(/*long timer*/) {

        this.timer = timer;

    }

    public static ServiceInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {

        System.setProperty("http.keepAlive", "false");
         httpClient = SSLTrust.trustcert(AppController.getInstance());
        httpClient.setReadTimeout(60, TimeUnit.SECONDS);
        httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Constant.SERVER_URL+Constant.DOMAINURL)
                .setClient(new OkClient(httpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ServiceInterface.class);

    }
}
