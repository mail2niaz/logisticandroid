package warehousemanagement.com.warehousemanagement;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import warehousemanagement.com.warehousemanagement.base.BaseActivity;
import warehousemanagement.com.warehousemanagement.model.responseobjects.TruckListModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.submitOrderModel;
import warehousemanagement.com.warehousemanagement.service.servicefactory.ServiceFactory;
import warehousemanagement.com.warehousemanagement.utils.CommonUtils;
import warehousemanagement.com.warehousemanagement.utils.Constant;

public class TruckListScreen extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.order_recycler_view)
    RecyclerView order_recycler_view;
    @Bind(R.id.truck_order_submit_txt)
    TextView truck_order_submit;


    int locationId;
    boolean OrderSubmitStatus;

    ArrayList<TruckListModel> orderarrayList = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_list_screen);
        ButterKnife.bind(this);

        truck_order_submit.setOnClickListener(this);
        locationId = Integer.parseInt(getIntent().getExtras().getString("LocationId"));
        OrderSubmitStatus = getIntent().getExtras().getBoolean("directOrderSubmitStatus");

        getTruckList(locationId);

    }

    private void getTruckList(int LocationId) {
        mProgressDialog.show();
        ServiceFactory.get().GetTruckList(LocationId, new Callback<List<TruckListModel>>() {
            @Override
            public void success(List<TruckListModel> truckListModels, Response response) {

                createOrderList(truckListModels);


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong

                mProgressDialog.dismiss();
                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                // mLogin.convertViewModel(loginModel);


            }
        });

    }

    private void createOrderList(List<TruckListModel> truckListModels) {

        Log.v(Constant.TAG, " Size --->" + truckListModels.size());
        if (truckListModels.size() > 0) {
            orderarrayList.clear();
            for (int i = 0; i < truckListModels.size(); i++) {
                Log.v(Constant.TAG, "Status :- " + truckListModels.get(i).getId());
                orderarrayList.add(truckListModels.get(i));

            }
            Log.v(Constant.TAG, "Size :- " + orderarrayList.size());

            order_recycler_view.setHasFixedSize(true);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            order_recycler_view.setLayoutManager(mLayoutManager);

            OrderAdapter dimension_adapter = new OrderAdapter();
            order_recycler_view.setAdapter(dimension_adapter);
            dimension_adapter.notifyDataSetChanged();
            mProgressDialog.dismiss();
        } else {
            mProgressDialog.dismiss();
            CommonUtils.ShowToast(this, "You dont have any order in this list.");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.truck_order_submit_txt:
                if (OrderSubmitStatus) {
                    getConformationProcess(Integer.parseInt(getIntent().getExtras().getString("OrderId")), getIntent().getExtras().getString("toStatus"));
                } else {
                    finish();
                }
                break;
        }
    }

    private void getConformationProcess(int orderId, String toStatusId) {

        Log.v(Constant.TAG, "Status Id: - " + String.valueOf(orderId));
        Log.v(Constant.TAG, "toStatusId :- " + toStatusId);
        ServiceFactory.get().submitOrder(String.valueOf(orderId), toStatusId, new Callback<submitOrderModel>() {
            @Override
            public void success(submitOrderModel loginModel, Response response) {
                // success!
                CommonUtils.ShowToast(TruckListScreen.this, "Order Submitted Successfully");
            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong

                CommonUtils.ShowToast(TruckListScreen.this, "Server not allow to submit the order. Please contact admin.");

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                // mLogin.convertViewModel(loginModel);


            }
        });
    }

    public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

        public OrderAdapter() {
            super();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.truck_card_item, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int postion) {

            Log.v(Constant.TAG, postion + " OrderId :- " + orderarrayList.get(postion).getId());
            viewHolder.card_truck_id_txt.setText(String.valueOf(orderarrayList.get(postion).getId()));
            Log.v(Constant.TAG, postion + " WareHouse :- " + orderarrayList.get(postion).getName());
            if (!CommonUtils.cutNull(orderarrayList.get(postion).getDescription()).equals("")) {
                viewHolder.description_txt.setText(orderarrayList.get(postion).getDescription());
            } else {
                viewHolder.description_txt.setText("No Note.");
            }

            viewHolder.truck_selection.setImageResource(R.drawable.radio_off);

          /*  viewHolder.truck_selection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtils.Log(TruckListScreen.this, "Cuurnet Selected Position :- " + postion);
                }
            });*/

            viewHolder.card_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonUtils.Log(TruckListScreen.this, "Current Selected Position :- " + postion);
                    for (int i = 0; i < orderarrayList.size(); i++) {
                        CommonUtils.Log(TruckListScreen.this, "Current i Position :- " + i);
                        if (i == postion) {
                            CommonUtils.Log(TruckListScreen.this, "Current i Selected Position :- " + i);
                            DetailOrderActivity.selectedTruckId = String.valueOf(orderarrayList.get(postion).getId());
                            viewHolder.truck_selection.setImageResource(R.drawable.radio_on);
                        } else {
                            viewHolder.truck_selection.setImageResource(R.drawable.radio_off);
                        }
                    }
                }
            });


          /*   if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderAddresses()).equals("")) {
                if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderAddresses().getAddress()).equals("")) {
                    Log.v(Constant.TAG, postion + " DeliveryAddress :- " + orderarrayList.get(postion).getOrderAddresses().getAddress());
                    viewHolder.address_txt.setText(orderarrayList.get(postion).getOrderAddresses().getAddress());
                }
            }

            if (!CommonUtils.cutNull(orderarrayList.get(postion).getOrderDate()).equals("")) {
                viewHolder.card_order_time.setText(orderarrayList.get(postion).getOrderDate());
            }


           viewHolder.history_fav_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });
            viewHolder.history_notes_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            viewHolder.history_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/

        }

        @Override
        public int getItemCount() {
            return orderarrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public TextView order_id_number_txt, Status_txt;
            public TextView card_truck_id_txt, description_txt, card_order_time, card_order_date;
            LinearLayout card_container;
            ImageView truck_selection;

            public ViewHolder(View itemView) {
                super(itemView);
                this.card_truck_id_txt = (TextView) itemView.findViewById(R.id.card_truck_id_name);
                this.description_txt = (TextView) itemView.findViewById(R.id.card_description_txt);
                this.truck_selection = (ImageView) itemView.findViewById(R.id.truck_select_img);
                this.card_container = (LinearLayout) itemView.findViewById(R.id.card_whole_container_linear);
            }
        }
    }

    /*private void getTruckList() {

        showProgress("Loading");

        ServiceFactory.get().OrderWorkFlow("DRIVER", new Callback<List<TabBarStatusModel>>() {
            @Override
            public void success(List<TabBarStatusModel> workFlowModelReponse, Response response) {
                // success!

                Log.v(Constant.TAG, "workFlowModelResponse --->" + workFlowModelReponse.size());

                for (int i = 0; i < workFlowModelReponse.size(); i++) {
                    Log.v(Constant.TAG, i + " Status Dec :- " + workFlowModelReponse.get(i).getStatusDesc());
                    Log.v(Constant.TAG, i + " Status Id :- " + workFlowModelReponse.get(i).getStatusId());
                    Log.v(Constant.TAG, i + " To Status Dec :- " + workFlowModelReponse.get(i).getToStatusId());
                    Log.v(Constant.TAG, i + " Id :- " + workFlowModelReponse.get(i).getId());
                }
                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }
                openHomeScreenAction(workFlowModelReponse);
                // startActivity(new Intent(LoginScreen.this, HomeScreen.class));
                //
                //  getWorkFlow();
                //   mLogin=new MLogin(BLogin.this,loginModel);


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                    Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                    Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                }

                showAlert("Error :- " + ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });
    }*/
}
