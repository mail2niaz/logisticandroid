package warehousemanagement.com.warehousemanagement;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import warehousemanagement.com.warehousemanagement.base.BaseActivity;
import warehousemanagement.com.warehousemanagement.model.requestobjects.LoginRequest;
import warehousemanagement.com.warehousemanagement.model.responseobjects.LoginModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.TabBarStatusModel;
import warehousemanagement.com.warehousemanagement.service.servicefactory.ServiceFactory;
import warehousemanagement.com.warehousemanagement.utils.AppController;
import warehousemanagement.com.warehousemanagement.utils.CommonUtils;
import warehousemanagement.com.warehousemanagement.utils.Constant;
import warehousemanagement.com.warehousemanagement.utils.ErrorMessage;
import warehousemanagement.com.warehousemanagement.utils.NetworkConnectionDetector;


public class LoginScreen extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.dont_have_account_txt)
    TextView dont_have_account_txt;
    @Bind(R.id.login_username_txt)
    TextView username_txt;
    @Bind(R.id.login_password_txt)
    TextView password_txt;
    @Bind(R.id.login_img)
    ImageView login_btn;

    NetworkConnectionDetector networkConnectionDetector;

    LoginRequest loginRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        ButterKnife.bind(this);

        networkConnectionDetector = new NetworkConnectionDetector(this);
        dont_have_account_txt.setText(Html.fromHtml("<p>DON'T HAVE AN ACCOUNT?<font size='7'color='#883e33'><bold>  SIGN UP</bold></font></p>"));

        login_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_img:
               // username_txt.setText("inventory@gmail.com");
                username_txt.setText("wh_driver@gmail.com");
                password_txt.setText("aa");
                if (username_txt.getText().toString().equals("")) {
                    CommonUtils.ShowToast(this, "Please enter username and try again");
                } else if (password_txt.getText().toString().equals("")) {
                    CommonUtils.ShowToast(this, "Please enter password and try again");
                } else {
                    if (networkConnectionDetector.connectionStatus()) {
                        LoginAction();
                    } else {
                        CommonUtils.ShowToast(this, "Network Connection is not response properly. Please check your connection.");
                    }
                    //startActivity(new Intent(this, HomeScreen.class));
                }
                break;
        }
    }

    public void LoginAction() {


        // showProgress(true);
        showProgress("Loading");


        ServiceFactory.get().Login(username_txt.getText().toString(), password_txt.getText().toString(), new Callback<LoginModel>() {
            @Override
            public void success(LoginModel loginModel, Response response) {
                // success!

                Log.v(Constant.TAG, "--->" + loginModel.getOrgUserId());
                Log.v(Constant.TAG, "Size --->" + loginModel.getOrderWorkflow().size());
                //startActivity(new Intent(LoginScreen.this, HomeScreen.class));
                //
                //getWorkFlow();
                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }
                AppController.getInstance().truckId = loginModel.getOrgUserId();
                openHomeScreenAction(loginModel.getOrderWorkflow());
                //   mLogin=new MLogin(BLogin.this,loginModel);

//                if (mProgressDialog.isShowing()) {
//
//                    mProgressDialog.dismiss();
//
//                }


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                showAlert(ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });

    }

    private void getWorkFlow() {

        showProgress("Loading");

        ServiceFactory.get().OrderWorkFlow("DRIVER", new Callback<List<TabBarStatusModel>>() {
            @Override
            public void success(List<TabBarStatusModel> workFlowModelReponse, Response response) {
                // success!

                Log.v(Constant.TAG, "workFlowModelResponse --->" + workFlowModelReponse.size());

                for (int i = 0; i < workFlowModelReponse.size(); i++) {
                    Log.v(Constant.TAG, i + " Status Dec :- " + workFlowModelReponse.get(i).getStatusDesc());
                    Log.v(Constant.TAG, i + " Status Id :- " + workFlowModelReponse.get(i).getStatusId());
                    Log.v(Constant.TAG, i + " To Status Dec :- " + workFlowModelReponse.get(i).getToStatusId());
                    Log.v(Constant.TAG, i + " Id :- " + workFlowModelReponse.get(i).getId());
                }
                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }
                openHomeScreenAction(workFlowModelReponse);
                // startActivity(new Intent(LoginScreen.this, HomeScreen.class));
                //
                //  getWorkFlow();
                //   mLogin=new MLogin(BLogin.this,loginModel);


            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {

                    mProgressDialog.dismiss();

                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                    Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                    Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                }

                showAlert("Error :- " + ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });
    }

    private void openHomeScreenAction(List<TabBarStatusModel> workFlowModelReponse) {
        finish();
        Intent openHomeScreen = new Intent(this, HomeScreen.class);
        openHomeScreen.putExtra("TabList", (Serializable) workFlowModelReponse);
        startActivity(openHomeScreen);
    }

}
