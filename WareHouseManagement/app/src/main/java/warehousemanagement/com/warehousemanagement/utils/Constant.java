package warehousemanagement.com.warehousemanagement.utils;


/**
 * Created by esakkimuthu on 1/05/2016.
 */


public class Constant {


    public static final String SERVER_URL = "http://106.51.0.40:8082/";
    public static final String DOMAINURL = "logistics-0.3.2/";

    public static final String TAG = "TruckTag";



    // GeoCode Constants
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "truckapp.com.truckapp";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";

    public static final String geoCodeKey = "AIzaSyBRzxEbHt7Kam5hRhgSU6AG0QKXnL_OP5U";

}


