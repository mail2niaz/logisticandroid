package warehousemanagement.com.warehousemanagement.model.responseobjects;


import java.util.List;

import warehousemanagement.com.warehousemanagement.model.responseobjects.login.CustomersModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.login.TrucksModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.login.UsersModel;

/**
 * Created by Nishanth on 24-09-2015.
 */
public class LoginModel {

    TrucksModel trucks;
    UsersModel users;
    CustomersModel customers;

    int orgUserId;
    String roleDesc;
    String firstName;
    String lastName;

    List<TabBarStatusModel> orderWorkflow;


    public TrucksModel getTrucks() {
        return trucks;
    }

    public void setTrucks(TrucksModel trucks) {
        this.trucks = trucks;
    }

    public UsersModel getUsers() {
        return users;
    }

    public void setUsers(UsersModel users) {
        this.users = users;
    }

    public CustomersModel getCustomers() {
        return customers;
    }

    public void setCustomers(CustomersModel customers) {
        this.customers = customers;
    }

    public List<TabBarStatusModel> getOrderWorkflow() {
        return orderWorkflow;
    }

    public void setOrderWorkflow(List<TabBarStatusModel> orderWorkflow) {
        this.orderWorkflow = orderWorkflow;
    }

    public int getOrgUserId() {
        return orgUserId;
    }

    public void setOrgUserId(int orgUserId) {
        this.orgUserId = orgUserId;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}