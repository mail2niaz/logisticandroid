package warehousemanagement.com.warehousemanagement.model.responseobjects;

/**
 * Created by Nishanth on 29-06-2016.
 */
public class TruckListModel {

    int id;
    String name;
    boolean activeFlag;
    String createdDate;
    String createdBy;
    int locationId;
    String helpDoc;
    String description;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getHelpDoc() {
        return helpDoc;
    }

    public void setHelpDoc(String helpDoc) {
        this.helpDoc = helpDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
