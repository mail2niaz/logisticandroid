package warehousemanagement.com.warehousemanagement.model;

/**
 * Created by esakkimuthu on 10/8/2015.
 */
public class AddressModel {

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    double lat,lon;
    String state,shortname;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;


    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    String county;


    public String getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getPrimaryid() {
        return primaryid;
    }

    public void setPrimaryid(String primaryid) {
        this.primaryid = primaryid;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    String isPrimary,primaryid,placeid;

    int score;
    int p_one;
    int p_two;
    int p_three;
    int p_four;
    int p_five;
    int p_six;
    int p_seven;
    int p_eight;
    public int getP_eight() {
        return p_eight;
    }

    public void setP_eight(int p_eight) {
        this.p_eight = p_eight;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getP_one() {
        return p_one;
    }

    public void setP_one(int p_one) {
        this.p_one = p_one;
    }

    public int getP_two() {
        return p_two;
    }

    public void setP_two(int p_two) {
        this.p_two = p_two;
    }

    public int getP_three() {
        return p_three;
    }

    public void setP_three(int p_three) {
        this.p_three = p_three;
    }

    public int getP_four() {
        return p_four;
    }

    public void setP_four(int p_four) {
        this.p_four = p_four;
    }

    public int getP_five() {
        return p_five;
    }

    public void setP_five(int p_five) {
        this.p_five = p_five;
    }

    public int getP_six() {
        return p_six;
    }

    public void setP_six(int p_six) {
        this.p_six = p_six;
    }

    public int getP_seven() {
        return p_seven;
    }

    public void setP_seven(int p_seven) {
        this.p_seven = p_seven;
    }

    String p_c_one;

    public String getP_c_one() {
        return p_c_one;
    }

    public void setP_c_one(String p_c_one) {
        this.p_c_one = p_c_one;
    }

    public String getP_c_two() {
        return p_c_two;
    }

    public void setP_c_two(String p_c_two) {
        this.p_c_two = p_c_two;
    }

    public String getP_c_three() {
        return p_c_three;
    }

    public void setP_c_three(String p_c_three) {
        this.p_c_three = p_c_three;
    }

    public String getP_c_four() {
        return p_c_four;
    }

    public void setP_c_four(String p_c_four) {
        this.p_c_four = p_c_four;
    }

    public String getP_c_five() {
        return p_c_five;
    }

    public void setP_c_five(String p_c_five) {
        this.p_c_five = p_c_five;
    }

    public String getP_c_six() {
        return p_c_six;
    }

    public void setP_c_six(String p_c_six) {
        this.p_c_six = p_c_six;
    }

    public String getP_c_seven() {
        return p_c_seven;
    }

    public void setP_c_seven(String p_c_seven) {
        this.p_c_seven = p_c_seven;
    }

    public String getP_c_eight() {
        return p_c_eight;
    }

    public void setP_c_eight(String p_c_eight) {
        this.p_c_eight = p_c_eight;
    }

    String p_c_two;
    String p_c_three;
    String p_c_four;
    String p_c_five;
    String p_c_six;
    String p_c_seven;
    String p_c_eight;

}
