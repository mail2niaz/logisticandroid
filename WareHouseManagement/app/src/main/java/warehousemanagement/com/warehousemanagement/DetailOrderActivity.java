package warehousemanagement.com.warehousemanagement;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import warehousemanagement.com.warehousemanagement.base.BaseActivity;
import warehousemanagement.com.warehousemanagement.model.OrderItemsModel;
import warehousemanagement.com.warehousemanagement.model.StaticOrderModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.OrderModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.orderItemsModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.submitOrderModel;
import warehousemanagement.com.warehousemanagement.service.servicefactory.ServiceFactory;
import warehousemanagement.com.warehousemanagement.utils.CommonUtils;
import warehousemanagement.com.warehousemanagement.utils.Constant;
import warehousemanagement.com.warehousemanagement.utils.ErrorMessage;


/**
 * Created by ramya on 6/15/2016.
 */
public class DetailOrderActivity extends BaseActivity implements View.OnClickListener {

    //    ShareAdapter shareAdapter;
    private List<ApplicationInfo> mAppList;
    private AppAdapter mAdapter;
    private SwipeMenuListView mListView;
    String TAG = "truck";

    ArrayList<OrderItemsModel> orderItems = new ArrayList<>();
    int[] images = {
            R.drawable.p1,
            R.drawable.p2,
            R.drawable.p3,
            R.drawable.p4,
            R.drawable.p5
    };

    @Bind(R.id.order_delivery_address_value_txt)
    TextView delivery_address;
    @Bind(R.id.order_header_txt)
    TextView order_header;
    @Bind(R.id.order_date_txt)
    TextView order_date_txt;
    @Bind(R.id.order_back_img)
    ImageView order_back_img;
    @Bind(R.id.order_submit_txt)
    TextView order_submit_txt;
    @Bind(R.id.order_truckno_txt)
    TextView order_truckno;
    @Bind(R.id.detail_order_location_img)
    ImageView location_img;
    @Bind(R.id.card_view)
    CardView truck_card_view;
    @Bind(R.id.order_truckno_value_txt)
    TextView order_truckno_value;

    String deliverydate, orderId, deliveryAddress;
    StaticOrderModel currentOrderModel = new StaticOrderModel();
    String toStatus = "";
    String ZipCode = "";
    String truckId = "";
    String Latitude, Longitude;
    boolean attachTruckDriverStatus = false;
    OrderModel WholeOrderModel;

    public static String selectedTruckId;

    // AlertDialog sportDialog;


    ArrayList<orderItemsModel> orderItemsList = null;
    ArrayList orderAcknowledgedStatus = null;
//    ArrayList<ShareValuesModel> shareValuesModels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailorder);
        ButterKnife.bind(this);

        orderItemsList = new ArrayList<orderItemsModel>();
        orderAcknowledgedStatus = new ArrayList();
        //sportDialog = new SpotsDialog(this);
        Bundle bundle = getIntent().getExtras();
        CommonUtils.Log(this, "Order Id inside :- " + getIntent().getExtras().getString("OrderId"));
        //insertArrayList(Integer.parseInt(bundle.getString("OrderId")));
        orderId = bundle.getString("OrderId");
        toStatus = bundle.getString("toStatus");
        order_header.setText("Order " + orderId);
        truckId = bundle.getString("truckId");
        selectedTruckId = truckId;
        WholeOrderModel = (OrderModel) bundle.getSerializable("WholeOrderModel");
        Latitude = bundle.getString("Latitude");
        Longitude = bundle.getString("Longitude");
        ZipCode = bundle.getString("ZipAddress");

        attachTruckDriverStatus = bundle.getBoolean("attachTruckDriverStatus");

        //if (!attachTruckDriverStatus) {
        //truck_card_view.setVisibility(View.VISIBLE);
        if (!truckId.equals("")) {
            order_truckno_value.setText(truckId);
        } else {
            order_truckno_value.setText("Choose Truck");
            order_truckno_value.setOnClickListener(this);
        }
        /*} else {
            truck_card_view.setVisibility(View.GONE);
        }*/

        if (!Latitude.equals("") && !Longitude.equals("")) {

        } else if (!ZipCode.equals("")) {

        } else {
            CommonUtils.ShowToast(this, "Ware Location is not available. Please contact admin.");
        }


        order_date_txt.setText(bundle.getString("date"));

        delivery_address.setText(bundle.getString("deliveraddress"));

        if (!bundle.getString("orderItemStatus").equals("")) {
            orderItemsList = (ArrayList<orderItemsModel>) bundle.getSerializable("orderItems");

            for (int i = 0; i < orderItemsList.size(); i++) {
                orderAcknowledgedStatus.add(i, false);
            }

            CommonUtils.Log(this, "orderItemsList :- " + orderItemsList.size());
            swipeListCreation();
        } else {

        }


        order_back_img.setOnClickListener(this);
        order_submit_txt.setOnClickListener(this);
        location_img.setOnClickListener(this);
        // currentOrderModel = bundle.getParcelable("currentSelectedOrderModel");

    }

    private void insertArrayList(int orderId) {

        OrderItemsModel orderItemsModel = new OrderItemsModel();
        orderItemsModel.setId(1120);
        orderItemsModel.setOrderId(orderId);
        orderItemsModel.setQuantity(1);
        orderItems.add(0, orderItemsModel);

        orderItemsModel = new OrderItemsModel();
        orderItemsModel.setId(1121);
        orderItemsModel.setOrderId(orderId);
        orderItemsModel.setQuantity(2);
        orderItems.add(1, orderItemsModel);

        orderItemsModel = new OrderItemsModel();
        orderItemsModel.setId(1122);
        orderItemsModel.setOrderId(orderId);
        orderItemsModel.setQuantity(1);
        orderItems.add(2, orderItemsModel);

        orderItemsModel = new OrderItemsModel();
        orderItemsModel.setId(1123);
        orderItemsModel.setOrderId(orderId);
        orderItemsModel.setQuantity(3);
        orderItems.add(3, orderItemsModel);

        orderItemsModel = new OrderItemsModel();
        orderItemsModel.setId(1124);
        orderItemsModel.setOrderId(orderId);
        orderItemsModel.setQuantity(1);
        orderItems.add(4, orderItemsModel);

        swipeListCreation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!selectedTruckId.equals("")) {
            order_truckno_value.setText(selectedTruckId);
        }
    }

    public void swipeListCreation() {

        Log.v(TAG, "Item swipeListCreation Called");
        mListView = (SwipeMenuListView) findViewById(R.id.order_slv);

        Log.v(TAG, "orderItemsList :- " + orderItemsList.size());

        mAdapter = new AppAdapter();
        mListView.setAdapter(mAdapter);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem acceptItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                acceptItem.setBackground(new ColorDrawable(getResources().getColor(R.color.yellow)));
                // set item width
                acceptItem.setWidth(dp2px(90));
                // set a icon
                acceptItem.setIcon(R.drawable.ok);
                // add to menu
                menu.addMenuItem(acceptItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.light_grey)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        mListView.setMenuCreator(creator);

        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                  /*  case 0:
                        // open
                        open(item);
                        break;*/
                    case 0:
                        //Toast.makeText(getApplicationContext(), position + " long click", Toast.LENGTH_SHORT).show();
                        orderAcknowledgedStatus.set(position, true);
//					delete(item);
                        break;
                    case 1:
                        // Toast.makeText(getApplicationContext(), position + " click ", Toast.LENGTH_SHORT).show();
                        orderAcknowledgedStatus.set(position, false);
                        break;
                }
                return false;
            }
        });

        // set SwipeListener
      /*  mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });*/

        // set MenuStateChangeListener
       /* mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });*/

        // other setting
//		listView.setCloseInterpolator(new BounceInterpolator());

        // test item long click
       /* mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Toast.makeText(getApplicationContext(), position + " long click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.order_back_img:
                finish();
                break;
            case R.id.order_submit_txt:
                Log.v(TAG, "Current orderId :- " + orderId);
                for (int k = 0; k < orderAcknowledgedStatus.size(); k++) {
                    if (orderAcknowledgedStatus.get(k) == false) {
                        Log.v(Constant.TAG, "Not allowed to submit");
                        CommonUtils.ShowToast(this, "Please Acknowledge all the project in the order.");
                        return;
                    }
                }
                submitOrder(orderId, toStatus);
                /*for (int i = 0; i < HomeScreen.OrderWholeArrayList.size(); i++) {
                    Log.v(TAG, "Order Id - " + HomeScreen.OrderWholeArrayList.get(i).getOrderId());
                    if (orderId.equals(HomeScreen.OrderWholeArrayList.get(i).getOrderId())) {
                        Log.v(TAG, "status Changed");
                        HomeScreen.OrderWholeArrayList.get(i).setStatus("r");
                        Log.v(TAG, "Current Status :- " + HomeScreen.OrderWholeArrayList.get(i).getStatus());
                    }
                }*/
                break;
            case R.id.order_truckno_value_txt:
                openTruckListScreen(WholeOrderModel);
                break;
            case R.id.detail_order_location_img:
                openMapScreen();
                break;
        }
    }

    private void openTruckListScreen(OrderModel selectedOrderModel) {

        Intent openTruckScreenActivity = new Intent(this, TruckListScreen.class);
        //openTruckScreenActivity.putExtra("CurrentViewHistoryModel", v_h_Model);
        Log.v(Constant.TAG, "Order Id outside :- " + selectedOrderModel.getId());

        openTruckScreenActivity.putExtra("LocationId", String.valueOf(selectedOrderModel.getLocationId()));
        openTruckScreenActivity.putExtra("date", selectedOrderModel.getOrderDate());
        openTruckScreenActivity.putExtra("toStatus", getIntent().getExtras().getString("toStatus"));
        openTruckScreenActivity.putExtra("directOrderSubmitStatus", false);

        if (!CommonUtils.cutNull(selectedOrderModel.getOrderAddresses()).equals("")) {

            Log.v(Constant.TAG, "Zip Code :- " + CommonUtils.cutNull(selectedOrderModel.getOrderAddresses().getZip()));

            if (CommonUtils.cutNull(selectedOrderModel.getOrderAddresses().getZip()).equals("")) {
                openTruckScreenActivity.putExtra("ZipAddress", selectedOrderModel.getOrderAddresses().getZip());
                openTruckScreenActivity.putExtra("Latitude", "");
                openTruckScreenActivity.putExtra("Longitude", "");
            } else {
                openTruckScreenActivity.putExtra("ZipAddress", "");
                openTruckScreenActivity.putExtra("Latitude", "");
                openTruckScreenActivity.putExtra("Longitude", "");
            }

            if (!CommonUtils.cutNull(selectedOrderModel.getOrderAddresses()).equals("")) {
                openTruckScreenActivity.putExtra("deliveraddress", selectedOrderModel.getOrderAddresses().getAddress());
            } else {
                openTruckScreenActivity.putExtra("deliveraddress", "Please Contact Ware house.");
            }
        } else {
            openTruckScreenActivity.putExtra("ZipAddress", "");
            openTruckScreenActivity.putExtra("Latitude", "");
            openTruckScreenActivity.putExtra("Longitude", "");
            openTruckScreenActivity.putExtra("deliveraddress", "Please Contact Ware house.");
        }

        if (selectedOrderModel.getOrderItems().size() > 0) {
            openTruckScreenActivity.putExtra("orderItemStatus", "present");
            openTruckScreenActivity.putExtra("orderItems", (Serializable) selectedOrderModel.getOrderItems());
        } else {
            openTruckScreenActivity.putExtra("orderItemStatus", "");
        }
        //openTruckScreenActivity.putExtra("currentSelectedOrderModel", selectedOrderModel);

        startActivity(openTruckScreenActivity);
        //startActivity(new Intent(getActivity(), DetailOrderActivity.class));
    }

    private void openMapScreen() {

        Intent openMap = new Intent(DetailOrderActivity.this, Mapscreen.class);
        openMap.putExtra("ZipAddress", ZipCode);
        openMap.putExtra("Latitude", Latitude);
        openMap.putExtra("Longitude", Longitude);
        startActivity(openMap);
        //ShareAddressToPalsScreenActivity.putExtra("CurrentViewHistoryModel", v_h_Model);
        /*Log.v(Constant.TAG, "Order Id outside :- " + selectedOrderModel.getId());

        ShareAddressToPalsScreenActivity.putExtra("OrderId", String.valueOf(selectedOrderModel.getId()));
        ShareAddressToPalsScreenActivity.putExtra("date", selectedOrderModel.getOrderDate());
        ShareAddressToPalsScreenActivity.putExtra("toStatus", HomeScreen.myList.get(currentViewTab).getToStatusId());

        if (!CommonUtils.cutNull(selectedOrderModel.getOrderAddresses()).equals("")) {
            ShareAddressToPalsScreenActivity.putExtra("deliveraddress", selectedOrderModel.getOrderAddresses().getAddress());
        } else {
            ShareAddressToPalsScreenActivity.putExtra("deliveraddress", "Please Contact Ware house.");
        }

        if (selectedOrderModel.getOrderItems().size() > 0) {
            ShareAddressToPalsScreenActivity.putExtra("orderItemStatus", "present");
            ShareAddressToPalsScreenActivity.putExtra("orderItems", (Serializable) selectedOrderModel.getOrderItems());
        } else {
            ShareAddressToPalsScreenActivity.putExtra("orderItemStatus", "");
        }
        //ShareAddressToPalsScreenActivity.putExtra("currentSelectedOrderModel", selectedOrderModel);
*/

        //startActivity(new Intent(getActivity(), DetailOrderActivity.class));
    }

    private void submitOrder(String orderId, String toStatus) {
        Log.v(Constant.TAG, "Status Id: - " + String.valueOf(orderId));
        Log.v(Constant.TAG, "toStatusId :- " + toStatus);
        mProgressDialog.show();
        ServiceFactory.get().submitOrder(orderId, toStatus, new Callback<submitOrderModel>() {
            @Override
            public void success(submitOrderModel loginModel, Response response) {
                // success!
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                Log.v(Constant.TAG, "--->" + loginModel.getId());
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                // something went wrong


                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                Log.v(Constant.TAG, "error :- " + error);
                Log.v(Constant.TAG, "error :- " + error.getMessage());
//                Log.v(Constant.TAG, "error :- " + error.getResponse().getHeaders().size());
                if (error.getResponse().getHeaders() != null) {
                    if (error.getResponse().getHeaders().size() > 0) {
                        for (int i = 0; i < error.getResponse().getHeaders().size(); i++) {
                            Log.v(Constant.TAG, i + " error :- " + error.getResponse().getHeaders().get(i).getName());
                            Log.v(Constant.TAG, i + " Values :- " + error.getResponse().getHeaders().get(i).getValue());
                        }
                    }
                }

                showAlert(ErrorMessage.server_busy);
                // mLogin.convertViewModel(loginModel);


            }
        });

    }

    class AppAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return orderItemsList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(),
                        R.layout.new_order_list_item, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
//            holder.quantity_txt.setText((item.loadLabel(getPackageManager())).toString().substring(0, 1));
//            holder.quantity_txt.setText("1");
            Log.v(TAG, "Item :- " + orderItemsList.get(position).getId());
            //String tete = String.valueOf(orderItems.get(position).getId());
            holder.jobNo_txt.setText(String.valueOf(orderItemsList.get(position).getId()));
            holder.poNo_txt.setText(String.valueOf(orderItemsList.get(position).getOrderId()));
            holder.quantity_txt.setText(String.valueOf(orderItemsList.get(position).getQuantity()));

            //holder.order_img.setImageResource(orderItemsList.get(position).getItemImage());
            if (!CommonUtils.cutNull(orderItemsList.get(position).getItemImage()).equals("")) {
                Glide.with(DetailOrderActivity.this).load(orderItemsList.get(position).getItemImage()).crossFade().into(holder.order_img);
            } else {
                holder.order_img.setImageResource(R.drawable.no_image);
            }

            /*holder.jobNo_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(DetailOrderActivity.this, "iv_icon_click", Toast.LENGTH_SHORT).show();
                }
            });*/

            return convertView;
        }

        class ViewHolder {
            TextView jobNo_txt;
            TextView poNo_txt;
            TextView quantity_txt;
            ImageView order_img;

            public ViewHolder(View view) {
                jobNo_txt = (TextView) view.findViewById(R.id.order_jobno_value_txt);
                poNo_txt = (TextView) view.findViewById(R.id.order_po_no_value_txt);
                quantity_txt = (TextView) view.findViewById(R.id.order_quantity_txt);
                order_img = (ImageView) view.findViewById(R.id.order_img);
                view.setTag(this);
            }
        }

       /* @Override
        public boolean getSwipEnableByPosition(int position) {
            if(position % 2 == 0){
                return false;
            }
            return true;
        }*/
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

}
