package warehousemanagement.com.warehousemanagement.service.interfacerespository;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import warehousemanagement.com.warehousemanagement.model.responseobjects.LoginModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.OrderModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.TabBarStatusModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.TruckListModel;
import warehousemanagement.com.warehousemanagement.model.responseobjects.submitOrderModel;


/**
 * Created by Ramya on 01/05/2016.
 */


public interface ServiceInterface {

    //*********************************LOGIN START*********************************************

    /**
     * LOGIN
     *
     * @param response
     */

    /*@FormUrlEncoded
    @POST("/api/User/Login")
    public void Login(@Field("username") String username, @Field("password") String password, Callback<LoginModel> response);*/


    /*@Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/Login/Authentication")
    public void Login(@Body LoginRequest username, Callback<LoginModel> response);*/
    //@Headers({"Content-Type: application/json;charset=UTF-8"})
    @FormUrlEncoded
    @POST("/login")
    public void Login(@Field("email") String email, @Field("password") String password, Callback<LoginModel> response);


    @FormUrlEncoded
    @POST("/order_workflow")
    public void OrderWorkFlow(@Field("role") String role, Callback<List<TabBarStatusModel>> response);


    @FormUrlEncoded
    @POST("/orders")
    public void GetOrderList(@Field("orgUserId") int orgUserId, @Field("status") String status, Callback<List<OrderModel>> response);


    @FormUrlEncoded
    @POST("/update_order")
    public void submitOrder(@Field("order_id") String order_id, @Field("order_status") String status, Callback<submitOrderModel> response);


    @FormUrlEncoded
    @POST("/org_trucks")
    public void GetTruckList(@Field("location_id") int locationId, Callback<List<TruckListModel>> response);
}