package warehousemanagement.com.warehousemanagement.model.requestobjects;

/**
 * Created by ramya on 12/28/2015.
 */
public class MobileBaseObjectModel {
//    "EmployeeId" : Alloy.Globals.EmployeeId,
//            "Mode" : "S",
        String svcGroup;
        String svcName;
        String OS_Type;
        String OS_version;
        String App_version;
        String App_ID;
        String ReferenceNumber;

    public String getSvcGroup() {
        return svcGroup;
    }

    public void setSvcGroup(String svcGroup) {
        this.svcGroup = svcGroup;
    }

    public String getSvcName() {
        return svcName;
    }

    public void setSvcName(String svcName) {
        this.svcName = svcName;
    }

    public String getOS_Type() {
        return OS_Type;
    }

    public void setOS_Type(String OS_Type) {
        this.OS_Type = OS_Type;
    }

    public String getOS_version() {
        return OS_version;
    }

    public void setOS_version(String OS_version) {
        this.OS_version = OS_version;
    }

    public String getApp_version() {
        return App_version;
    }

    public void setApp_version(String app_version) {
        App_version = app_version;
    }

    public String getApp_ID() {
        return App_ID;
    }

    public void setApp_ID(String app_ID) {
        App_ID = app_ID;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }
}
